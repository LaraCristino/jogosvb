﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_maiormenor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_num = New System.Windows.Forms.TextBox()
        Me.lbl_maior = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txt_num2 = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbl_menor = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btn_mostrar = New System.Windows.Forms.Button()
        Me.btn_limpar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Location = New System.Drawing.Point(128, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(227, 46)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Maior e Menor"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label2.Location = New System.Drawing.Point(23, 81)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 23)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Digite um número:"
        '
        'txt_num
        '
        Me.txt_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_num.Location = New System.Drawing.Point(57, 117)
        Me.txt_num.Multiline = True
        Me.txt_num.Name = "txt_num"
        Me.txt_num.Size = New System.Drawing.Size(57, 33)
        Me.txt_num.TabIndex = 4
        '
        'lbl_maior
        '
        Me.lbl_maior.BackColor = System.Drawing.Color.GhostWhite
        Me.lbl_maior.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_maior.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lbl_maior.Location = New System.Drawing.Point(63, 299)
        Me.lbl_maior.Name = "lbl_maior"
        Me.lbl_maior.Size = New System.Drawing.Size(62, 36)
        Me.lbl_maior.TabIndex = 10
        Me.lbl_maior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label3.Location = New System.Drawing.Point(23, 264)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(148, 23)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "O maior número é:"
        '
        'txt_num2
        '
        Me.txt_num2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_num2.Location = New System.Drawing.Point(254, 117)
        Me.txt_num2.Multiline = True
        Me.txt_num2.Name = "txt_num2"
        Me.txt_num2.Size = New System.Drawing.Size(57, 33)
        Me.txt_num2.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(205, 81)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(165, 23)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Digite outro  número:"
        '
        'lbl_menor
        '
        Me.lbl_menor.BackColor = System.Drawing.Color.GhostWhite
        Me.lbl_menor.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_menor.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lbl_menor.Location = New System.Drawing.Point(262, 299)
        Me.lbl_menor.Name = "lbl_menor"
        Me.lbl_menor.Size = New System.Drawing.Size(62, 36)
        Me.lbl_menor.TabIndex = 14
        Me.lbl_menor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label6.Location = New System.Drawing.Point(222, 264)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(148, 23)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "O menor número é:"
        '
        'btn_mostrar
        '
        Me.btn_mostrar.BackColor = System.Drawing.Color.MediumOrchid
        Me.btn_mostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_mostrar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btn_mostrar.Location = New System.Drawing.Point(146, 193)
        Me.btn_mostrar.Name = "btn_mostrar"
        Me.btn_mostrar.Size = New System.Drawing.Size(91, 38)
        Me.btn_mostrar.TabIndex = 15
        Me.btn_mostrar.Text = "Mostrar"
        Me.btn_mostrar.UseVisualStyleBackColor = False
        '
        'btn_limpar
        '
        Me.btn_limpar.BackColor = System.Drawing.Color.MediumOrchid
        Me.btn_limpar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_limpar.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btn_limpar.Location = New System.Drawing.Point(146, 372)
        Me.btn_limpar.Name = "btn_limpar"
        Me.btn_limpar.Size = New System.Drawing.Size(103, 30)
        Me.btn_limpar.TabIndex = 16
        Me.btn_limpar.Text = "Limpar"
        Me.btn_limpar.UseVisualStyleBackColor = False
        '
        'frm_maiormenor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(417, 424)
        Me.Controls.Add(Me.btn_limpar)
        Me.Controls.Add(Me.btn_mostrar)
        Me.Controls.Add(Me.lbl_menor)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.txt_num2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lbl_maior)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txt_num)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_maiormenor"
        Me.Text = "frm_maiormenor"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_num As System.Windows.Forms.TextBox
    Friend WithEvents lbl_maior As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txt_num2 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lbl_menor As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btn_mostrar As System.Windows.Forms.Button
    Friend WithEvents btn_limpar As System.Windows.Forms.Button
End Class
