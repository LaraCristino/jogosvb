﻿Public Class frm_Quadrado
    Dim res, lado As Double
    Private Sub btn_calcular_Click(sender As Object, e As EventArgs) Handles btn_calcular.Click
        lado = txt_lado.Text
        res = (lado * lado)
        lbl_area.Text = res
    End Sub

    Private Sub btn_limpar_Click(sender As Object, e As EventArgs) Handles btn_limpar.Click
        txt_lado.Clear()
        lbl_area.Text = ""
        txt_lado.Focus()
    End Sub
End Class