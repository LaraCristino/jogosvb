﻿Public Class frm_maiormenor
    Dim n1, n2, maior, menor As Integer
    Private Sub btn_mostrar_Click(sender As Object, e As EventArgs) Handles btn_mostrar.Click
        n1 = txt_num.Text
        n2 = txt_num2.Text

        If n1 > n2 Then
            maior = n1
            menor = n2
        Else
            maior = n2
            menor = n1
        End If

        lbl_maior.Text = maior
        lbl_menor.Text = menor
    End Sub

    Private Sub btn_limpar_Click(sender As Object, e As EventArgs) Handles btn_limpar.Click
        txt_num.Clear()
        txt_num2.Clear()
        lbl_maior.Text = ""
        lbl_menor.Text = ""
        txt_num.Focus()

    End Sub
End Class