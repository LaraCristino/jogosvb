﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_calculadora
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txt_um = New System.Windows.Forms.TextBox()
        Me.txt_dois = New System.Windows.Forms.TextBox()
        Me.rdb_adicao = New System.Windows.Forms.RadioButton()
        Me.rdb_subtracao = New System.Windows.Forms.RadioButton()
        Me.rdb_multiplicacao = New System.Windows.Forms.RadioButton()
        Me.rdb_divisao = New System.Windows.Forms.RadioButton()
        Me.lbl_operacao = New System.Windows.Forms.Label()
        Me.lbl_um = New System.Windows.Forms.Label()
        Me.lbl_dois = New System.Windows.Forms.Label()
        Me.lbl_resultado = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btn_calcular = New System.Windows.Forms.Button()
        Me.btn_limpa = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txt_um
        '
        Me.txt_um.Location = New System.Drawing.Point(22, 109)
        Me.txt_um.Name = "txt_um"
        Me.txt_um.Size = New System.Drawing.Size(117, 20)
        Me.txt_um.TabIndex = 0
        '
        'txt_dois
        '
        Me.txt_dois.Location = New System.Drawing.Point(22, 161)
        Me.txt_dois.Name = "txt_dois"
        Me.txt_dois.Size = New System.Drawing.Size(117, 20)
        Me.txt_dois.TabIndex = 1
        '
        'rdb_adicao
        '
        Me.rdb_adicao.AutoSize = True
        Me.rdb_adicao.Location = New System.Drawing.Point(182, 92)
        Me.rdb_adicao.Name = "rdb_adicao"
        Me.rdb_adicao.Size = New System.Drawing.Size(58, 17)
        Me.rdb_adicao.TabIndex = 3
        Me.rdb_adicao.TabStop = True
        Me.rdb_adicao.Text = "Adição"
        Me.rdb_adicao.UseVisualStyleBackColor = True
        '
        'rdb_subtracao
        '
        Me.rdb_subtracao.AutoSize = True
        Me.rdb_subtracao.Location = New System.Drawing.Point(182, 115)
        Me.rdb_subtracao.Name = "rdb_subtracao"
        Me.rdb_subtracao.Size = New System.Drawing.Size(74, 17)
        Me.rdb_subtracao.TabIndex = 4
        Me.rdb_subtracao.TabStop = True
        Me.rdb_subtracao.Text = "Subtração"
        Me.rdb_subtracao.UseVisualStyleBackColor = True
        '
        'rdb_multiplicacao
        '
        Me.rdb_multiplicacao.AutoSize = True
        Me.rdb_multiplicacao.Location = New System.Drawing.Point(182, 138)
        Me.rdb_multiplicacao.Name = "rdb_multiplicacao"
        Me.rdb_multiplicacao.Size = New System.Drawing.Size(87, 17)
        Me.rdb_multiplicacao.TabIndex = 5
        Me.rdb_multiplicacao.TabStop = True
        Me.rdb_multiplicacao.Text = "Multiplicação"
        Me.rdb_multiplicacao.UseVisualStyleBackColor = True
        '
        'rdb_divisao
        '
        Me.rdb_divisao.AutoSize = True
        Me.rdb_divisao.Location = New System.Drawing.Point(182, 161)
        Me.rdb_divisao.Name = "rdb_divisao"
        Me.rdb_divisao.Size = New System.Drawing.Size(60, 17)
        Me.rdb_divisao.TabIndex = 6
        Me.rdb_divisao.TabStop = True
        Me.rdb_divisao.Text = "Divisão"
        Me.rdb_divisao.UseVisualStyleBackColor = True
        '
        'lbl_operacao
        '
        Me.lbl_operacao.AutoSize = True
        Me.lbl_operacao.Location = New System.Drawing.Point(179, 74)
        Me.lbl_operacao.Name = "lbl_operacao"
        Me.lbl_operacao.Size = New System.Drawing.Size(57, 13)
        Me.lbl_operacao.TabIndex = 7
        Me.lbl_operacao.Text = "Operação:"
        '
        'lbl_um
        '
        Me.lbl_um.AutoSize = True
        Me.lbl_um.Location = New System.Drawing.Point(19, 90)
        Me.lbl_um.Name = "lbl_um"
        Me.lbl_um.Size = New System.Drawing.Size(84, 13)
        Me.lbl_um.TabIndex = 8
        Me.lbl_um.Text = "Primeiro Número"
        '
        'lbl_dois
        '
        Me.lbl_dois.AutoSize = True
        Me.lbl_dois.Location = New System.Drawing.Point(19, 136)
        Me.lbl_dois.Name = "lbl_dois"
        Me.lbl_dois.Size = New System.Drawing.Size(88, 13)
        Me.lbl_dois.TabIndex = 9
        Me.lbl_dois.Text = "Segundo número"
        '
        'lbl_resultado
        '
        Me.lbl_resultado.AutoSize = True
        Me.lbl_resultado.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lbl_resultado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_resultado.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lbl_resultado.Location = New System.Drawing.Point(137, 227)
        Me.lbl_resultado.Name = "lbl_resultado"
        Me.lbl_resultado.Size = New System.Drawing.Size(2, 15)
        Me.lbl_resultado.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label1.Location = New System.Drawing.Point(69, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(142, 46)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Calculadora"
        '
        'btn_calcular
        '
        Me.btn_calcular.Location = New System.Drawing.Point(94, 187)
        Me.btn_calcular.Name = "btn_calcular"
        Me.btn_calcular.Size = New System.Drawing.Size(94, 24)
        Me.btn_calcular.TabIndex = 13
        Me.btn_calcular.Text = "CALCULAR"
        Me.btn_calcular.UseVisualStyleBackColor = True
        '
        'btn_limpa
        '
        Me.btn_limpa.Location = New System.Drawing.Point(209, 1)
        Me.btn_limpa.Name = "btn_limpa"
        Me.btn_limpa.Size = New System.Drawing.Size(75, 23)
        Me.btn_limpa.TabIndex = 14
        Me.btn_limpa.Text = "Limpar"
        Me.btn_limpa.UseVisualStyleBackColor = True
        '
        'frm_calculadora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.btn_limpa)
        Me.Controls.Add(Me.btn_calcular)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbl_resultado)
        Me.Controls.Add(Me.lbl_dois)
        Me.Controls.Add(Me.lbl_um)
        Me.Controls.Add(Me.lbl_operacao)
        Me.Controls.Add(Me.rdb_divisao)
        Me.Controls.Add(Me.rdb_multiplicacao)
        Me.Controls.Add(Me.rdb_subtracao)
        Me.Controls.Add(Me.rdb_adicao)
        Me.Controls.Add(Me.txt_dois)
        Me.Controls.Add(Me.txt_um)
        Me.Name = "frm_calculadora"
        Me.Text = "calculadora"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_um As TextBox
    Friend WithEvents txt_dois As TextBox
    Friend WithEvents rdb_adicao As RadioButton
    Friend WithEvents rdb_subtracao As RadioButton
    Friend WithEvents rdb_multiplicacao As RadioButton
    Friend WithEvents rdb_divisao As RadioButton
    Friend WithEvents lbl_operacao As Label
    Friend WithEvents lbl_um As Label
    Friend WithEvents lbl_dois As Label
    Friend WithEvents lbl_resultado As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents btn_calcular As Button
    Friend WithEvents btn_limpa As Button
End Class
