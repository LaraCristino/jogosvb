﻿Imports System.Text

Public Class frm_jogodaforca
    Dim indiceCorpo As Integer
    Dim corpo(5) As PictureBox

    Private Sub btn_jogar_Click(sender As Object, e As EventArgs) Handles btn_jogar.Click

        If Trim(txt_palavra.Text) = "" Or Trim(txt_dica.Text) = "" Then
            lbl_invalido.Text = "Digite a palavra e a dica corretamente"
            lbl_invalido.Visible = True
        Else
            'novo jogo!
            lbl_invalido.Text = ""
            btn_jogar.Enabled = False
            txt_dica.Enabled = False
            txt_palavra.Enabled = False
            grp_letras.Visible = True
            lbl_palavra.Visible = True
            LetrasDigitadas.Visible = True
            lbl_letras_digitadas.Visible = True

            lbl_palavra.Text = ""
            For i As Integer = 1 To txt_palavra.Text.Length
                lbl_palavra.Text = lbl_palavra.Text + "_ "
            Next
        End If

    End Sub

    Private Sub frm_jogodaforca_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        visibilidadeInicialDoBoneco()

        lbl_invalido.Visible = False
        lbl_palavra.Visible = False
        grp_letras.Visible = False
        lbl_mensagem.Visible = False
        btn_jogar_novamente.Visible = False
        LetrasDigitadas.Visible = False
        lbl_letras_digitadas.Visible = False

        corpo(0) = img_cabeca
        corpo(1) = img_tronco
        corpo(2) = img_perna1
        corpo(3) = img_perna2
        corpo(4) = img_braco1
        corpo(5) = img_braco2

        'cria efetivamente os controles em tela para que o foco possa ser setado
        Me.Show()
        txt_palavra.Focus()
    End Sub

    Private Sub executaRodada(letra As String)
        Dim posLetras As New List(Of Integer)

        'varre todos chars, encontrando ocorrencias da letra
        For chr As Integer = 0 To txt_palavra.Text.Length - 1
            If UCase(txt_palavra.Text.Chars(chr)) = UCase(letra) Then
                posLetras.Add(chr)
            End If
        Next

        If posLetras.Count > 0 Then
            'letra valida
            For Each c In posLetras
                Dim novaPalavra As New StringBuilder(lbl_palavra.Text)
                novaPalavra(c * 2) = UCase(letra)
                lbl_palavra.Text = novaPalavra.ToString()
            Next

            If lbl_palavra.Text.IndexOf("_") = -1 Then
                'jogo acabou. venceu!
                lbl_mensagem.Text = "Voce venceu!"
                lbl_mensagem.Visible = True
                grp_letras.Enabled = False
                btn_jogar_novamente.Visible = True
            End If
        Else
            'letra invalida
            If indiceCorpo < 5 Then
                corpo(indiceCorpo).Visible = True
                indiceCorpo = indiceCorpo + 1
                LetrasDigitadas.Items.Add(letra)
            Else
                'jogo acabou. perdeu!

                'exibe bracinho faltante
                corpo(5).Visible = True

                lbl_mensagem.Text = "Voce perdeu!"
                lbl_mensagem.Visible = True
                grp_letras.Enabled = False
                btn_jogar_novamente.Visible = True
            End If
        End If
    End Sub

    Private Sub btn_jogar_novamente_Click(sender As Object, e As EventArgs) Handles btn_jogar_novamente.Click
        'zerar jogo, voltando ao estado inicial
        btn_jogar.Enabled = True
        txt_dica.Enabled = True
        txt_dica.Text = ""
        txt_palavra.Enabled = True
        txt_palavra.Text = ""
        grp_letras.Visible = True
        lbl_palavra.Visible = False
        lbl_invalido.Text = ""

        visibilidadeInicialDoBoneco()
        lbl_invalido.Visible = False
        lbl_palavra.Visible = False
        grp_letras.Visible = False
        lbl_mensagem.Visible = False
        btn_jogar_novamente.Visible = False

        LetrasDigitadas.Items.Clear()
        LetrasDigitadas.Visible = False
        lbl_letras_digitadas.Visible = False

        grp_letras.Enabled = True

        txt_palavra.Focus()
    End Sub

    Private Sub visibilidadeInicialDoBoneco()
        indiceCorpo = 0
        img_cabeca.Visible = False
        img_braco1.Visible = False
        img_braco2.Visible = False
        img_perna1.Visible = False
        img_perna2.Visible = False
        img_tronco.Visible = False
    End Sub

    Private Sub btn_a_Click(sender As Object, e As EventArgs) Handles btn_a.Click
        executaRodada("A")
    End Sub

    Private Sub btn_b_Click(sender As Object, e As EventArgs) Handles btn_b.Click
        executaRodada("B")
    End Sub

    Private Sub btn_c_Click(sender As Object, e As EventArgs) Handles btn_c.Click
        executaRodada("C")
    End Sub

    Private Sub btn_d_Click(sender As Object, e As EventArgs) Handles btn_d.Click
        executaRodada("D")
    End Sub

    Private Sub btn_e_Click(sender As Object, e As EventArgs) Handles btn_e.Click
        executaRodada("E")
    End Sub

    Private Sub btn_f_Click(sender As Object, e As EventArgs) Handles btn_f.Click
        executaRodada("F")
    End Sub

    Private Sub btn_g_Click(sender As Object, e As EventArgs) Handles btn_g.Click
        executaRodada("G")
    End Sub

    Private Sub btn_h_Click(sender As Object, e As EventArgs) Handles btn_h.Click
        executaRodada("H")
    End Sub

    Private Sub btn_i_Click(sender As Object, e As EventArgs) Handles btn_i.Click
        executaRodada("I")
    End Sub

    Private Sub btn_j_Click(sender As Object, e As EventArgs) Handles btn_j.Click
        executaRodada("J")
    End Sub

    Private Sub btn_k_Click(sender As Object, e As EventArgs) Handles btn_k.Click
        executaRodada("K")
    End Sub

    Private Sub btn_l_Click(sender As Object, e As EventArgs) Handles btn_l.Click
        executaRodada("L")
    End Sub

    Private Sub btn_m_Click(sender As Object, e As EventArgs) Handles btn_m.Click
        executaRodada("M")
    End Sub

    Private Sub btn_n_Click(sender As Object, e As EventArgs) Handles btn_n.Click
        executaRodada("N")
    End Sub

    Private Sub btn_o_Click(sender As Object, e As EventArgs) Handles btn_o.Click
        executaRodada("O")
    End Sub

    Private Sub btn_p_Click(sender As Object, e As EventArgs) Handles btn_p.Click
        executaRodada("P")
    End Sub

    Private Sub btn_q_Click(sender As Object, e As EventArgs) Handles btn_q.Click
        executaRodada("Q")
    End Sub

    Private Sub btn_r_Click(sender As Object, e As EventArgs) Handles btn_r.Click
        executaRodada("R")
    End Sub

    Private Sub btn_s_Click(sender As Object, e As EventArgs) Handles btn_s.Click
        executaRodada("S")
    End Sub

    Private Sub btn_t_Click(sender As Object, e As EventArgs) Handles btn_t.Click
        executaRodada("T")
    End Sub

    Private Sub btn_u_Click(sender As Object, e As EventArgs) Handles btn_u.Click
        executaRodada("U")
    End Sub

    Private Sub btn_v_Click(sender As Object, e As EventArgs) Handles btn_v.Click
        executaRodada("V")
    End Sub

    Private Sub btn_w_Click(sender As Object, e As EventArgs) Handles btn_w.Click
        executaRodada("W")
    End Sub

    Private Sub btn_x_Click(sender As Object, e As EventArgs) Handles btn_x.Click
        executaRodada("X")
    End Sub

    Private Sub btn_y_Click(sender As Object, e As EventArgs) Handles btn_y.Click
        executaRodada("Y")
    End Sub

    Private Sub btn_z_Click(sender As Object, e As EventArgs) Handles btn_z.Click
        executaRodada("Z")
    End Sub

End Class