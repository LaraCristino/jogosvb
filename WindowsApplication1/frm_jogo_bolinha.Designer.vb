﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frm_jogo_bolinha
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_jogo_bolinha))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_nome = New System.Windows.Forms.TextBox()
        Me.cmb_rodadas = New System.Windows.Forms.ComboBox()
        Me.btn_go = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lbl_qtdd = New System.Windows.Forms.Label()
        Me.lbl_rodada = New System.Windows.Forms.Label()
        Me.lbl_pc = New System.Windows.Forms.Label()
        Me.lbl_cpu = New System.Windows.Forms.Label()
        Me.lbl_jogador = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbl_resultado = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lbl_input_invalido = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.img_um = New System.Windows.Forms.PictureBox()
        Me.img_tres = New System.Windows.Forms.PictureBox()
        Me.img_smile = New System.Windows.Forms.PictureBox()
        Me.img_dois = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        CType(Me.img_um, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_tres, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_smile, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_dois, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label1.Location = New System.Drawing.Point(2, 242)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(174, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Digite o nome do jogador:"
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ButtonFace
        Me.Label2.Location = New System.Drawing.Point(2, 272)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(255, 21)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Selecione a quantidade de rodadas:"
        '
        'txt_nome
        '
        Me.txt_nome.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_nome.Location = New System.Drawing.Point(246, 237)
        Me.txt_nome.Name = "txt_nome"
        Me.txt_nome.Size = New System.Drawing.Size(214, 20)
        Me.txt_nome.TabIndex = 2
        '
        'cmb_rodadas
        '
        Me.cmb_rodadas.FormattingEnabled = True
        Me.cmb_rodadas.Location = New System.Drawing.Point(246, 269)
        Me.cmb_rodadas.Name = "cmb_rodadas"
        Me.cmb_rodadas.Size = New System.Drawing.Size(213, 21)
        Me.cmb_rodadas.TabIndex = 3
        '
        'btn_go
        '
        Me.btn_go.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btn_go.Location = New System.Drawing.Point(509, 237)
        Me.btn_go.Name = "btn_go"
        Me.btn_go.Size = New System.Drawing.Size(79, 53)
        Me.btn_go.TabIndex = 4
        Me.btn_go.Text = "GO!"
        Me.btn_go.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.GroupBox1.Controls.Add(Me.lbl_qtdd)
        Me.GroupBox1.Controls.Add(Me.lbl_rodada)
        Me.GroupBox1.Controls.Add(Me.lbl_pc)
        Me.GroupBox1.Controls.Add(Me.lbl_cpu)
        Me.GroupBox1.Controls.Add(Me.lbl_jogador)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(245, 296)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(213, 120)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Placar Geral"
        '
        'lbl_qtdd
        '
        Me.lbl_qtdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_qtdd.Location = New System.Drawing.Point(134, 95)
        Me.lbl_qtdd.Name = "lbl_qtdd"
        Me.lbl_qtdd.Size = New System.Drawing.Size(53, 16)
        Me.lbl_qtdd.TabIndex = 10
        '
        'lbl_rodada
        '
        Me.lbl_rodada.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_rodada.Location = New System.Drawing.Point(134, 72)
        Me.lbl_rodada.Name = "lbl_rodada"
        Me.lbl_rodada.Size = New System.Drawing.Size(53, 16)
        Me.lbl_rodada.TabIndex = 9
        '
        'lbl_pc
        '
        Me.lbl_pc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_pc.Location = New System.Drawing.Point(134, 50)
        Me.lbl_pc.Name = "lbl_pc"
        Me.lbl_pc.Size = New System.Drawing.Size(53, 16)
        Me.lbl_pc.TabIndex = 8
        '
        'lbl_cpu
        '
        Me.lbl_cpu.AutoSize = True
        Me.lbl_cpu.Location = New System.Drawing.Point(134, 50)
        Me.lbl_cpu.Name = "lbl_cpu"
        Me.lbl_cpu.Size = New System.Drawing.Size(0, 15)
        Me.lbl_cpu.TabIndex = 5
        '
        'lbl_jogador
        '
        Me.lbl_jogador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_jogador.Location = New System.Drawing.Point(134, 28)
        Me.lbl_jogador.Name = "lbl_jogador"
        Me.lbl_jogador.Size = New System.Drawing.Size(53, 16)
        Me.lbl_jogador.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 98)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 15)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Qtdd de Rodadas:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 75)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 15)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Rodada Atual:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 15)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "CPU"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Jogador:"
        '
        'lbl_resultado
        '
        Me.lbl_resultado.AutoSize = True
        Me.lbl_resultado.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lbl_resultado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_resultado.Font = New System.Drawing.Font("Gill Sans Ultra Bold", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_resultado.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.lbl_resultado.Location = New System.Drawing.Point(12, 324)
        Me.lbl_resultado.Name = "lbl_resultado"
        Me.lbl_resultado.Size = New System.Drawing.Size(148, 25)
        Me.lbl_resultado.TabIndex = 4
        Me.lbl_resultado.Text = "[RESULTADO]"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Red
        Me.Label12.Location = New System.Drawing.Point(225, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(246, 37)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Encontre o Smile:"
        '
        'lbl_input_invalido
        '
        Me.lbl_input_invalido.AutoSize = True
        Me.lbl_input_invalido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_input_invalido.Location = New System.Drawing.Point(245, 218)
        Me.lbl_input_invalido.Name = "lbl_input_invalido"
        Me.lbl_input_invalido.Size = New System.Drawing.Size(12, 16)
        Me.lbl_input_invalido.TabIndex = 17
        Me.lbl_input_invalido.Text = "."
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'img_um
        '
        Me.img_um.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.img_um.BackColor = System.Drawing.Color.Transparent
        Me.img_um.BackgroundImage = CType(resources.GetObject("img_um.BackgroundImage"), System.Drawing.Image)
        Me.img_um.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.img_um.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.img_um.Location = New System.Drawing.Point(43, 73)
        Me.img_um.Name = "img_um"
        Me.img_um.Padding = New System.Windows.Forms.Padding(20)
        Me.img_um.Size = New System.Drawing.Size(196, 132)
        Me.img_um.TabIndex = 19
        Me.img_um.TabStop = False
        '
        'img_tres
        '
        Me.img_tres.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.img_tres.BackColor = System.Drawing.Color.Transparent
        Me.img_tres.BackgroundImage = CType(resources.GetObject("img_tres.BackgroundImage"), System.Drawing.Image)
        Me.img_tres.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.img_tres.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.img_tres.Location = New System.Drawing.Point(447, 73)
        Me.img_tres.Name = "img_tres"
        Me.img_tres.Padding = New System.Windows.Forms.Padding(20)
        Me.img_tres.Size = New System.Drawing.Size(192, 132)
        Me.img_tres.TabIndex = 20
        Me.img_tres.TabStop = False
        '
        'img_smile
        '
        Me.img_smile.BackColor = System.Drawing.Color.Transparent
        Me.img_smile.BackgroundImage = CType(resources.GetObject("img_smile.BackgroundImage"), System.Drawing.Image)
        Me.img_smile.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.img_smile.Location = New System.Drawing.Point(507, 361)
        Me.img_smile.Name = "img_smile"
        Me.img_smile.Size = New System.Drawing.Size(153, 156)
        Me.img_smile.TabIndex = 21
        Me.img_smile.TabStop = False
        '
        'img_dois
        '
        Me.img_dois.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.img_dois.BackColor = System.Drawing.Color.Transparent
        Me.img_dois.BackgroundImage = CType(resources.GetObject("img_dois.BackgroundImage"), System.Drawing.Image)
        Me.img_dois.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.img_dois.Cursor = System.Windows.Forms.Cursors.SizeAll
        Me.img_dois.Location = New System.Drawing.Point(245, 73)
        Me.img_dois.Name = "img_dois"
        Me.img_dois.Padding = New System.Windows.Forms.Padding(20)
        Me.img_dois.Size = New System.Drawing.Size(196, 132)
        Me.img_dois.TabIndex = 22
        Me.img_dois.TabStop = False
        '
        'frm_jogo_bolinha
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(669, 535)
        Me.Controls.Add(Me.lbl_resultado)
        Me.Controls.Add(Me.img_smile)
        Me.Controls.Add(Me.img_dois)
        Me.Controls.Add(Me.img_tres)
        Me.Controls.Add(Me.img_um)
        Me.Controls.Add(Me.lbl_input_invalido)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btn_go)
        Me.Controls.Add(Me.cmb_rodadas)
        Me.Controls.Add(Me.txt_nome)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "frm_jogo_bolinha"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Jogo da Bolinha"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.img_um, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_tres, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_smile, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_dois, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_nome As TextBox
    Friend WithEvents cmb_rodadas As ComboBox
    Friend WithEvents btn_go As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lbl_jogador As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lbl_resultado As Label
    Friend WithEvents lbl_cpu As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents lbl_input_invalido As Label
    Friend WithEvents Timer1 As Windows.Forms.Timer
    Friend WithEvents img_um As PictureBox
    Friend WithEvents img_tres As PictureBox
    Friend WithEvents img_smile As PictureBox
    Friend WithEvents lbl_qtdd As Label
    Friend WithEvents lbl_rodada As Label
    Friend WithEvents lbl_pc As Label
    Friend WithEvents img_dois As PictureBox
End Class
