﻿Public Class frm_media
    Dim n1, n2, n3, n4, res As Double

    Private Sub frm_media_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btn_media_Click(sender As Object, e As EventArgs) Handles btn_media.Click
        n1 = txt_n1.Text
        n2 = txt_n2.Text
        n3 = txt_n3.Text
        n4 = txt_n4.Text
        res = (n1 + n2 + n3 + n4) / 4
        lbl_media.Text = res
        If (res > 7) Then
            lbl_aprova.Text = "Aprovado"
        ElseIf (res < 7) And (res > 4) Then
            lbl_aprova.Text = "Exame"
        Else
            lbl_aprova.Text = "Reprovado"

        End If
    End Sub

    Private Sub btn_limpar_Click(sender As Object, e As EventArgs) Handles btn_limpar.Click
        txt_n1.Clear()
        txt_n2.Clear()
        txt_n3.Clear()
        txt_n4.Clear()
        lbl_media.Text = ""
        txt_n1.Focus()

    End Sub
End Class