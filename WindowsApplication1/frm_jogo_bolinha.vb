﻿Public Class frm_jogo_bolinha
    Dim tempo As Integer
    Dim xlocation(2) As Integer
    Dim positionSmiley As Integer
    Dim partidaEmAndamento As Boolean
    Dim rodadaAtual As Integer = 0

    Private Sub btn_go_Click(sender As Object, e As EventArgs) Handles btn_go.Click

        lbl_input_invalido.Text = ""
        lbl_resultado.Text = ""
        lbl_jogador.Text = 0
        lbl_pc.Text = 0
        lbl_rodada.Text = 0
        rodadaAtual = 0

        If Not partidaEmAndamento Then
            If Trim(txt_nome.Text) = "" Or cmb_rodadas.SelectedItem = Nothing Then
                lbl_input_invalido.Text = "Seu n00b; digita direito!"
            Else
                lbl_input_invalido.Text = ""

                partidaEmAndamento = True
                btn_go.Enabled = Not partidaEmAndamento
                execucaoDaPartida()
            End If

        End If

    End Sub

    Private Sub execucaoDaPartida()

        rodadaAtual = rodadaAtual + 1
        lbl_rodada.Text = rodadaAtual

        'esconder smiley
        img_smile.Visible = False

        'associar smiley a um copo randomicamente
        positionSmiley = CInt(Math.Ceiling(Rnd() * 2))

        Dim x As Integer
        tempo = 8

        While tempo > 0
            x = xlocation(0)
            xlocation(0) = xlocation(1)
            xlocation(1) = xlocation(2)
            xlocation(2) = x

            img_um.Location = New Point(xlocation(0), 73)
            img_dois.Location = New Point(xlocation(1), 73)
            img_tres.Location = New Point(xlocation(2), 73)

            tempo = tempo - 1

            Application.DoEvents()
            System.Threading.Thread.Sleep(200)
        End While

    End Sub

    Private Sub frm_jogo_bolinha_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmb_rodadas.Items.Add(3)
        cmb_rodadas.Items.Add(4)
        cmb_rodadas.Items.Add(5)
        cmb_rodadas.Items.Add(6)
        cmb_rodadas.Items.Add(7)
        cmb_rodadas.Items.Add(8)
        cmb_rodadas.Items.Add(9)
        cmb_rodadas.Items.Add(10)

        xlocation(0) = 43
        xlocation(1) = 245
        xlocation(2) = 447

        lbl_input_invalido.Text = ""
        lbl_resultado.Text = ""

        img_smile.Visible = False

        partidaEmAndamento = False

    End Sub

    Private Sub img_tres_Click(sender As Object, e As EventArgs) Handles img_tres.Click
        exibirSmiley()
        ajustaPlacar(acertou(2))
    End Sub

    Private Sub img_dois_Click(sender As Object, e As EventArgs) Handles img_dois.Click
        exibirSmiley()
        ajustaPlacar(acertou(1))
    End Sub

    Private Sub img_um_Click(sender As Object, e As EventArgs) Handles img_um.Click
        exibirSmiley()
        ajustaPlacar(acertou(0))
    End Sub

    Private Sub exibirSmiley()
        img_smile.Location = New Point(xlocation(positionSmiley), 73)
        img_smile.Visible = True
    End Sub

    Private Function acertou(valor)
        Return positionSmiley = valor
    End Function

    Private Sub ajustaPlacar(usuarioAcertou)
        If usuarioAcertou Then
            lbl_jogador.Text = CInt(lbl_jogador.Text) + 1
        Else
            lbl_pc.Text = CInt(lbl_pc.Text) + 1
        End If

        Application.DoEvents()
        System.Threading.Thread.Sleep(2000)

        partidaEmAndamento = (rodadaAtual < cmb_rodadas.SelectedItem)
        If partidaEmAndamento Then
            execucaoDaPartida()
        Else
            btn_go.Enabled = True
            lbl_resultado.Visible = True

            If CInt(lbl_jogador.Text) > CInt(lbl_pc.Text) Then
                lbl_resultado.Text = "Voce ganhou!"
            ElseIf CInt(lbl_jogador.Text) < CInt(lbl_pc.Text) Then
                lbl_resultado.Text = "Voce perdeu, n00b!"
            Else
                lbl_resultado.Text = "Empate!"
            End If
        End If
    End Sub

    Private Sub cmb_rodadas_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmb_rodadas.SelectedIndexChanged
        lbl_qtdd.Text = DirectCast(sender, ComboBox).SelectedItem
    End Sub
End Class