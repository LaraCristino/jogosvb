﻿Public Class frm_numeros_primos
    Dim num As Integer
    Private Sub btn_ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click
        execute()
    End Sub
    Private Sub execute()
        If (Trim(txt_numero.Text) = "") Then
            lbl_resultado.Text = "Digite um numero valido"
        Else
            num = CInt(txt_numero.Text)
            If validarNumeroPrimo(num) = True Then
                lbl_resultado.Text = "É número primo"
            Else
                lbl_resultado.Text = "Não é número primo"
            End If
        End If
    End Sub

    Private Function validarNumeroPrimo(n As Integer)
        'a function (funcoes gerais) retorna valor eo sub (trabalhar com campos do formulário) nao
        Dim i As Integer
        For i = 2 To n / 2
            If n Mod i = 0 Then
                Return False
            End If
        Next

        Return True
    End Function


End Class