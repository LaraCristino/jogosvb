﻿Public Class frm_Triangulo
    Dim base, alt, area As Double
    Private Sub btn_calcular_Click(sender As Object, e As EventArgs) Handles btn_calcular.Click
        base = txt_base.Text
        alt = txt_altura.Text
        area = (base * alt) / 2
        lbl_area.Text = area
    End Sub

    Private Sub btn_limpar_Click(sender As Object, e As EventArgs) Handles btn_limpar.Click
        txt_altura.Clear()
        txt_base.Clear()
        lbl_area.Text = ""
        txt_base.Focus()
    End Sub
End Class