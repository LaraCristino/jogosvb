﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_media
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_n1 = New System.Windows.Forms.TextBox()
        Me.txt_n2 = New System.Windows.Forms.TextBox()
        Me.txt_n3 = New System.Windows.Forms.TextBox()
        Me.txt_n4 = New System.Windows.Forms.TextBox()
        Me.btn_media = New System.Windows.Forms.Button()
        Me.lbl_media = New System.Windows.Forms.Label()
        Me.btn_limpar = New System.Windows.Forms.Button()
        Me.lbl_aprova = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(211, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 46)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Média Aritmética"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(40, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "1º Nota:"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(180, 77)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 23)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "2º Nota:"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(328, 77)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 23)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "3º Nota:"
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(472, 77)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 23)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "4º Nota:"
        '
        'txt_n1
        '
        Me.txt_n1.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_n1.Location = New System.Drawing.Point(55, 107)
        Me.txt_n1.Multiline = True
        Me.txt_n1.Name = "txt_n1"
        Me.txt_n1.Size = New System.Drawing.Size(45, 31)
        Me.txt_n1.TabIndex = 6
        Me.txt_n1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_n2
        '
        Me.txt_n2.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_n2.Location = New System.Drawing.Point(199, 107)
        Me.txt_n2.Multiline = True
        Me.txt_n2.Name = "txt_n2"
        Me.txt_n2.Size = New System.Drawing.Size(45, 31)
        Me.txt_n2.TabIndex = 7
        Me.txt_n2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_n3
        '
        Me.txt_n3.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_n3.Location = New System.Drawing.Point(343, 107)
        Me.txt_n3.Multiline = True
        Me.txt_n3.Name = "txt_n3"
        Me.txt_n3.Size = New System.Drawing.Size(45, 31)
        Me.txt_n3.TabIndex = 8
        Me.txt_n3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txt_n4
        '
        Me.txt_n4.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_n4.Location = New System.Drawing.Point(490, 107)
        Me.txt_n4.Multiline = True
        Me.txt_n4.Name = "txt_n4"
        Me.txt_n4.Size = New System.Drawing.Size(45, 31)
        Me.txt_n4.TabIndex = 9
        Me.txt_n4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btn_media
        '
        Me.btn_media.BackColor = System.Drawing.Color.DimGray
        Me.btn_media.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_media.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_media.Location = New System.Drawing.Point(151, 189)
        Me.btn_media.Name = "btn_media"
        Me.btn_media.Size = New System.Drawing.Size(121, 42)
        Me.btn_media.TabIndex = 10
        Me.btn_media.Text = "Média"
        Me.btn_media.UseVisualStyleBackColor = False
        '
        'lbl_media
        '
        Me.lbl_media.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.lbl_media.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_media.Location = New System.Drawing.Point(312, 185)
        Me.lbl_media.Name = "lbl_media"
        Me.lbl_media.Size = New System.Drawing.Size(76, 54)
        Me.lbl_media.TabIndex = 11
        Me.lbl_media.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_limpar
        '
        Me.btn_limpar.BackColor = System.Drawing.Color.DimGray
        Me.btn_limpar.Font = New System.Drawing.Font("Verdana", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_limpar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_limpar.Location = New System.Drawing.Point(284, 258)
        Me.btn_limpar.Name = "btn_limpar"
        Me.btn_limpar.Size = New System.Drawing.Size(136, 31)
        Me.btn_limpar.TabIndex = 12
        Me.btn_limpar.Text = "Limpar"
        Me.btn_limpar.UseVisualStyleBackColor = False
        '
        'lbl_aprova
        '
        Me.lbl_aprova.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lbl_aprova.Location = New System.Drawing.Point(456, 185)
        Me.lbl_aprova.Name = "lbl_aprova"
        Me.lbl_aprova.Size = New System.Drawing.Size(106, 54)
        Me.lbl_aprova.TabIndex = 13
        '
        'frm_media
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.RosyBrown
        Me.ClientSize = New System.Drawing.Size(609, 312)
        Me.Controls.Add(Me.lbl_aprova)
        Me.Controls.Add(Me.btn_limpar)
        Me.Controls.Add(Me.lbl_media)
        Me.Controls.Add(Me.btn_media)
        Me.Controls.Add(Me.txt_n4)
        Me.Controls.Add(Me.txt_n3)
        Me.Controls.Add(Me.txt_n2)
        Me.Controls.Add(Me.txt_n1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_media"
        Me.Text = "frm_media"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_n1 As System.Windows.Forms.TextBox
    Friend WithEvents txt_n2 As System.Windows.Forms.TextBox
    Friend WithEvents txt_n3 As System.Windows.Forms.TextBox
    Friend WithEvents txt_n4 As System.Windows.Forms.TextBox
    Friend WithEvents btn_media As System.Windows.Forms.Button
    Friend WithEvents lbl_media As System.Windows.Forms.Label
    Friend WithEvents btn_limpar As System.Windows.Forms.Button
    Friend WithEvents lbl_aprova As Label
End Class
