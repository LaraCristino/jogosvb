﻿Public Class frm_jokenpo

    Dim elementos(2) As String
    Dim rodadaAtual As Integer = 0

    Private Sub btn_jogar_Click(sender As Object, e As EventArgs) Handles btn_jogar.Click
        If Trim(txt_nome.Text) = "" Or cmb_rodadas.SelectedItem = Nothing Then
            lbl_mensagem.Text = "Informe os campos, n00b!"
        Else
            lbl_mensagem.Text = ""
            btn_jogar_novamente.Visible = False
            lbl_qtdd.Text = cmb_rodadas.SelectedItem

            btn_jogar.Enabled = False
            txt_nome.Enabled = False
            cmb_rodadas.Enabled = False

            rdb_papel.Enabled = True
            rdb_pedra.Enabled = True
            rdb_tesoura.Enabled = True
        End If
    End Sub

    Public Function jokenpo(opcaoSelecionada As String)
        If rodadaAtual < cmb_rodadas.SelectedItem - 1 Then
            Dim usuario = jogadaUsuario(opcaoSelecionada)
            Dim cpu = jogadaCPU()
            resultado(usuario, cpu)
            rodadaAtual = rodadaAtual + 1
            lbl_rodada_atual.Text = rodadaAtual
        ElseIf rodadaAtual = cmb_rodadas.SelectedItem - 1 Then
            Dim usuario = jogadaUsuario(opcaoSelecionada)
            Dim cpu = jogadaCPU()
            resultado(usuario, cpu)
            rodadaAtual = rodadaAtual + 1
            lbl_rodada_atual.Text = rodadaAtual
            fimDeJogo()
        Else
            fimDeJogo()
        End If
    End Function

    Private Sub fimDeJogo()
        lbl_mensagem.Text = "Fim de jogo! "

        Dim j = CInt(lbl_jogador.Text)
        Dim c = CInt(lbl_pc.Text)
        Dim e = CInt(lbl_empate.Text)

        If j > c And j > e Then
            lbl_mensagem.Text = lbl_mensagem.Text + "Voce venceu!"
        ElseIf j < c And c > e Then
            lbl_mensagem.Text = lbl_mensagem.Text + "Voce perdeu para o CPU :("
        Else
            lbl_mensagem.Text = lbl_mensagem.Text + "Empate :|"
        End If

        rdb_papel.Enabled = False
        rdb_papel.Checked = False
        rdb_pedra.Enabled = False
        rdb_pedra.Checked = False
        rdb_tesoura.Enabled = False
        rdb_tesoura.Checked = False

        btn_jogar_novamente.Visible = True
    End Sub

    Private Function jogadaUsuario(jogada As String)
        If jogada = "Pedra" Then
            img_pedra.Visible = True
            img_papel.Visible = False
            img_tesoura.Visible = False

            img_pedra.Location = New Point(137, 20)
            Return "Pedra"
        ElseIf jogada = "Papel" Then
            img_pedra.Visible = False
            img_papel.Visible = True
            img_tesoura.Visible = False

            img_papel.Location = New Point(137, 20)
            Return "Papel"
        ElseIf jogada = "Tesoura" Then
            img_pedra.Visible = False
            img_papel.Visible = False
            img_tesoura.Visible = True

            img_tesoura.Location = New Point(137, 20)
            Return "Tesoura"
        End If
    End Function

    Private Function jogadaCPU()
        Dim jogada As String = elementos(CInt(Math.Ceiling(Rnd() * 3)) - 1)
        If jogada = "Pedra" Then
            img_pedra_cpu.Visible = True
            img_papel_cpu.Visible = False
            img_tesoura_cpu.Visible = False

            img_pedra_cpu.Location = New Point(23, 26)
            Return "Pedra"
        ElseIf jogada = "Papel" Then
            img_pedra_cpu.Visible = False
            img_papel_cpu.Visible = True
            img_tesoura_cpu.Visible = False

            img_papel_cpu.Location = New Point(23, 26)
            Return "Papel"
        ElseIf jogada = "Tesoura" Then
            img_pedra_cpu.Visible = False
            img_papel_cpu.Visible = False
            img_tesoura_cpu.Visible = True

            img_tesoura_cpu.Location = New Point(23, 26)
            Return "Tesoura"
        End If
    End Function

    Private Sub resultado(usuario As String, cpu As String)
        If usuario = "Pedra" And cpu = "Pedra" Then
            lbl_empate.Text = CInt(lbl_empate.Text) + 1
        ElseIf usuario = "Pedra" And cpu = "Papel" Then
            lbl_pc.Text = CInt(lbl_pc.Text) + 1
        ElseIf usuario = "Pedra" And cpu = "Tesoura" Then
            lbl_jogador.Text = CInt(lbl_jogador.Text) + 1
        ElseIf usuario = "Papel" And cpu = "Pedra" Then
            lbl_jogador.Text = CInt(lbl_jogador.Text) + 1
        ElseIf usuario = "Papel" And cpu = "Papel" Then
            lbl_empate.Text = CInt(lbl_empate.Text) + 1
        ElseIf usuario = "Papel" And cpu = "Tesoura" Then
            lbl_pc.Text = CInt(lbl_pc.Text) + 1
        ElseIf usuario = "Tesoura" And cpu = "Pedra" Then
            lbl_pc.Text = CInt(lbl_pc.Text) + 1
        ElseIf usuario = "Tesoura" And cpu = "Papel" Then
            lbl_jogador.Text = CInt(lbl_jogador.Text) + 1
        ElseIf usuario = "Tesoura" And cpu = "Tesoura" Then
            lbl_empate.Text = CInt(lbl_empate.Text) + 1
        End If
    End Sub

    Private Sub frm_jokenpo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        elementos(0) = "Pedra"
        elementos(1) = "Papel"
        elementos(2) = "Tesoura"

        cmb_rodadas.Items.Add(3)
        cmb_rodadas.Items.Add(4)
        cmb_rodadas.Items.Add(5)
        cmb_rodadas.Items.Add(6)
        cmb_rodadas.Items.Add(7)
        cmb_rodadas.Items.Add(8)
        cmb_rodadas.Items.Add(9)
        cmb_rodadas.Items.Add(10)

        img_pedra.Visible = False
        img_papel.Visible = False
        img_tesoura.Visible = False
        img_pedra_cpu.Visible = False
        img_papel_cpu.Visible = False
        img_tesoura_cpu.Visible = False

        rdb_papel.Enabled = False
        rdb_pedra.Enabled = False
        rdb_tesoura.Enabled = False

        lbl_empate.Text = 0
        lbl_jogador.Text = 0
        lbl_pc.Text = 0

    End Sub



    Private Sub rdb_pedra_Click(sender As Object, e As EventArgs) Handles rdb_pedra.Click
        jokenpo("Pedra")
    End Sub

    Private Sub rdb_papel_Click(sender As Object, e As EventArgs) Handles rdb_papel.Click
        jokenpo("Papel")
    End Sub

    Private Sub rdb_tesoura_Click(sender As Object, e As EventArgs) Handles rdb_tesoura.Click
        jokenpo("Tesoura")
    End Sub

    Private Sub btn_jogar_novamente_Click(sender As Object, e As EventArgs) Handles btn_jogar_novamente.Click
        txt_nome.Enabled = True
        txt_nome.Text = ""
        cmb_rodadas.Enabled = True
        cmb_rodadas.SelectedItem = Nothing
        btn_jogar.Enabled = True

        img_pedra.Visible = False
        img_papel.Visible = False
        img_tesoura.Visible = False
        img_pedra_cpu.Visible = False
        img_papel_cpu.Visible = False
        img_tesoura_cpu.Visible = False

        lbl_jogador.Text = 0
        lbl_pc.Text = 0
        lbl_empate.Text = 0
        lbl_rodada_atual.Text = 0
        lbl_qtdd.Text = 0

        btn_jogar_novamente.Visible = False
        lbl_mensagem.Text = ""

        rodadaAtual = 0
    End Sub
End Class