﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_paresimpares
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_num = New System.Windows.Forms.TextBox()
        Me.btn_mostrar = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lbl_resp = New System.Windows.Forms.Label()
        Me.btn_limpar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(127, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 46)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Pares e Impares"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(25, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(203, 23)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Digite um número:"
        '
        'txt_num
        '
        Me.txt_num.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txt_num.Location = New System.Drawing.Point(238, 86)
        Me.txt_num.Multiline = True
        Me.txt_num.Name = "txt_num"
        Me.txt_num.Size = New System.Drawing.Size(169, 45)
        Me.txt_num.TabIndex = 5
        '
        'btn_mostrar
        '
        Me.btn_mostrar.BackColor = System.Drawing.Color.MediumOrchid
        Me.btn_mostrar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_mostrar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_mostrar.Location = New System.Drawing.Point(131, 137)
        Me.btn_mostrar.Name = "btn_mostrar"
        Me.btn_mostrar.Size = New System.Drawing.Size(161, 59)
        Me.btn_mostrar.TabIndex = 16
        Me.btn_mostrar.Text = "Mostrar Resultado"
        Me.btn_mostrar.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(25, 221)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(203, 23)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "O número digitado é:"
        '
        'lbl_resp
        '
        Me.lbl_resp.BackColor = System.Drawing.Color.GhostWhite
        Me.lbl_resp.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_resp.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lbl_resp.Location = New System.Drawing.Point(234, 213)
        Me.lbl_resp.Name = "lbl_resp"
        Me.lbl_resp.Size = New System.Drawing.Size(173, 36)
        Me.lbl_resp.TabIndex = 18
        Me.lbl_resp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btn_limpar
        '
        Me.btn_limpar.BackColor = System.Drawing.Color.MediumOrchid
        Me.btn_limpar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_limpar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_limpar.Location = New System.Drawing.Point(338, 2)
        Me.btn_limpar.Name = "btn_limpar"
        Me.btn_limpar.Size = New System.Drawing.Size(103, 30)
        Me.btn_limpar.TabIndex = 19
        Me.btn_limpar.Text = "Limpar"
        Me.btn_limpar.UseVisualStyleBackColor = False
        '
        'frm_paresimpares
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Plum
        Me.ClientSize = New System.Drawing.Size(444, 263)
        Me.Controls.Add(Me.btn_limpar)
        Me.Controls.Add(Me.lbl_resp)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btn_mostrar)
        Me.Controls.Add(Me.txt_num)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_paresimpares"
        Me.Text = "frm_paresimpares"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txt_num As System.Windows.Forms.TextBox
    Friend WithEvents btn_mostrar As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lbl_resp As System.Windows.Forms.Label
    Friend WithEvents btn_limpar As System.Windows.Forms.Button
End Class
