﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_menu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_menu))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ProgramasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BásicosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ÁreaDoQuadradoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ÁreaDoTrianguloToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MédiaAritméticaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalculadoraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NúmerosMaiorEMenorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NúmerosParesEImparesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NùmerosAntecessorESucessorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntermediáriosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AvançadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EncerrarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SairToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProgramasToolStripMenuItem, Me.EncerrarToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(284, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ProgramasToolStripMenuItem
        '
        Me.ProgramasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BásicosToolStripMenuItem, Me.IntermediáriosToolStripMenuItem, Me.AvançadosToolStripMenuItem})
        Me.ProgramasToolStripMenuItem.Name = "ProgramasToolStripMenuItem"
        Me.ProgramasToolStripMenuItem.Size = New System.Drawing.Size(76, 20)
        Me.ProgramasToolStripMenuItem.Text = "&Programas"
        '
        'BásicosToolStripMenuItem
        '
        Me.BásicosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ÁreaDoQuadradoToolStripMenuItem, Me.ÁreaDoTrianguloToolStripMenuItem, Me.MédiaAritméticaToolStripMenuItem, Me.CalculadoraToolStripMenuItem, Me.NúmerosMaiorEMenorToolStripMenuItem, Me.NúmerosParesEImparesToolStripMenuItem, Me.NùmerosAntecessorESucessorToolStripMenuItem})
        Me.BásicosToolStripMenuItem.Name = "BásicosToolStripMenuItem"
        Me.BásicosToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BásicosToolStripMenuItem.Text = "Básicos"
        '
        'ÁreaDoQuadradoToolStripMenuItem
        '
        Me.ÁreaDoQuadradoToolStripMenuItem.Name = "ÁreaDoQuadradoToolStripMenuItem"
        Me.ÁreaDoQuadradoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.ÁreaDoQuadradoToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.ÁreaDoQuadradoToolStripMenuItem.Text = "Área do Quadrado"
        '
        'ÁreaDoTrianguloToolStripMenuItem
        '
        Me.ÁreaDoTrianguloToolStripMenuItem.Image = CType(resources.GetObject("ÁreaDoTrianguloToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ÁreaDoTrianguloToolStripMenuItem.Name = "ÁreaDoTrianguloToolStripMenuItem"
        Me.ÁreaDoTrianguloToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.ÁreaDoTrianguloToolStripMenuItem.Text = "Área do Triangulo"
        '
        'MédiaAritméticaToolStripMenuItem
        '
        Me.MédiaAritméticaToolStripMenuItem.Name = "MédiaAritméticaToolStripMenuItem"
        Me.MédiaAritméticaToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.MédiaAritméticaToolStripMenuItem.Text = "Média Aritmética"
        '
        'CalculadoraToolStripMenuItem
        '
        Me.CalculadoraToolStripMenuItem.Image = CType(resources.GetObject("CalculadoraToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CalculadoraToolStripMenuItem.Name = "CalculadoraToolStripMenuItem"
        Me.CalculadoraToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.CalculadoraToolStripMenuItem.Text = "Calculadora"
        '
        'NúmerosMaiorEMenorToolStripMenuItem
        '
        Me.NúmerosMaiorEMenorToolStripMenuItem.Image = CType(resources.GetObject("NúmerosMaiorEMenorToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NúmerosMaiorEMenorToolStripMenuItem.Name = "NúmerosMaiorEMenorToolStripMenuItem"
        Me.NúmerosMaiorEMenorToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.NúmerosMaiorEMenorToolStripMenuItem.Text = "Números - Maior e Menor"
        '
        'NúmerosParesEImparesToolStripMenuItem
        '
        Me.NúmerosParesEImparesToolStripMenuItem.Name = "NúmerosParesEImparesToolStripMenuItem"
        Me.NúmerosParesEImparesToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.NúmerosParesEImparesToolStripMenuItem.Text = "Números - Pares e Impares"
        '
        'NùmerosAntecessorESucessorToolStripMenuItem
        '
        Me.NùmerosAntecessorESucessorToolStripMenuItem.Name = "NùmerosAntecessorESucessorToolStripMenuItem"
        Me.NùmerosAntecessorESucessorToolStripMenuItem.Size = New System.Drawing.Size(250, 22)
        Me.NùmerosAntecessorESucessorToolStripMenuItem.Text = "Nùmeros - Antecessor e Sucessor"
        '
        'IntermediáriosToolStripMenuItem
        '
        Me.IntermediáriosToolStripMenuItem.Name = "IntermediáriosToolStripMenuItem"
        Me.IntermediáriosToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.IntermediáriosToolStripMenuItem.Text = "Intermediários"
        '
        'AvançadosToolStripMenuItem
        '
        Me.AvançadosToolStripMenuItem.Name = "AvançadosToolStripMenuItem"
        Me.AvançadosToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AvançadosToolStripMenuItem.Text = "Avançados"
        '
        'EncerrarToolStripMenuItem
        '
        Me.EncerrarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SairToolStripMenuItem})
        Me.EncerrarToolStripMenuItem.Name = "EncerrarToolStripMenuItem"
        Me.EncerrarToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.EncerrarToolStripMenuItem.Text = "&Encerrar"
        '
        'SairToolStripMenuItem
        '
        Me.SairToolStripMenuItem.Name = "SairToolStripMenuItem"
        Me.SairToolStripMenuItem.Size = New System.Drawing.Size(93, 22)
        Me.SairToolStripMenuItem.Text = "Sair"
        '
        'frm_menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frm_menu"
        Me.Text = "Menu"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ProgramasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BásicosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ÁreaDoQuadradoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ÁreaDoTrianguloToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MédiaAritméticaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalculadoraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NúmerosMaiorEMenorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NúmerosParesEImparesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NùmerosAntecessorESucessorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IntermediáriosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AvançadosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EncerrarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SairToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem

End Class
