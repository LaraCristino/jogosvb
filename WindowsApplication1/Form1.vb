﻿Public Class frm_menu
    Dim resp As String
    Private Sub SairToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SairToolStripMenuItem.Click
        resp = MsgBox("Deseja sair?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "AVISO")
        If resp = MsgBoxResult.Yes Then
            Me.Dispose() 'encerra o processo'
        End If
    End Sub

    Private Sub ÁreaDoQuadradoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ÁreaDoQuadradoToolStripMenuItem.Click
        frm_Quadrado.ShowDialog()
    End Sub

    Private Sub ÁreaDoTrianguloToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ÁreaDoTrianguloToolStripMenuItem.Click
        frm_Triangulo.ShowDialog()
    End Sub

    Private Sub MédiaAritméticaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MédiaAritméticaToolStripMenuItem.Click
        frm_media.ShowDialog()
    End Sub

    Private Sub NùmerosAntecessorESucessorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NùmerosAntecessorESucessorToolStripMenuItem.Click
        frm_antsuc.ShowDialog()
    End Sub

    Private Sub NúmerosMaiorEMenorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NúmerosMaiorEMenorToolStripMenuItem.Click
        frm_maiormenor.ShowDialog()
    End Sub

    Private Sub NúmerosParesEImparesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NúmerosParesEImparesToolStripMenuItem.Click
        frm_paresimpares.ShowDialog()
    End Sub

    Private Sub frm_calculadora_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

End Class
