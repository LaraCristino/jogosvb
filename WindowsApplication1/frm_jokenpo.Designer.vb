﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jokenpo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_jokenpo))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_nome = New System.Windows.Forms.TextBox()
        Me.cmb_rodadas = New System.Windows.Forms.ComboBox()
        Me.grp_jogador = New System.Windows.Forms.GroupBox()
        Me.rdb_tesoura = New System.Windows.Forms.RadioButton()
        Me.img_tesoura = New System.Windows.Forms.PictureBox()
        Me.rdb_papel = New System.Windows.Forms.RadioButton()
        Me.img_papel = New System.Windows.Forms.PictureBox()
        Me.rdb_pedra = New System.Windows.Forms.RadioButton()
        Me.img_pedra = New System.Windows.Forms.PictureBox()
        Me.grp_cpu = New System.Windows.Forms.GroupBox()
        Me.img_tesoura_cpu = New System.Windows.Forms.PictureBox()
        Me.img_papel_cpu = New System.Windows.Forms.PictureBox()
        Me.img_pedra_cpu = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.btn_jogar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lbl_rodada_atual = New System.Windows.Forms.Label()
        Me.lbl_empate_texto = New System.Windows.Forms.Label()
        Me.lbl_qtdd = New System.Windows.Forms.Label()
        Me.lbl_empate = New System.Windows.Forms.Label()
        Me.lbl_pc = New System.Windows.Forms.Label()
        Me.lbl_cpu = New System.Windows.Forms.Label()
        Me.lbl_jogador = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lbl_mensagem = New System.Windows.Forms.Label()
        Me.btn_jogar_novamente = New System.Windows.Forms.Button()
        Me.grp_jogador.SuspendLayout()
        CType(Me.img_tesoura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_papel, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_pedra, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grp_cpu.SuspendLayout()
        CType(Me.img_tesoura_cpu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_papel_cpu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_pedra_cpu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(40, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Digite o nome do Jogador:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(157, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Digite a quantidade de partidas:"
        '
        'txt_nome
        '
        Me.txt_nome.Location = New System.Drawing.Point(212, 49)
        Me.txt_nome.Name = "txt_nome"
        Me.txt_nome.Size = New System.Drawing.Size(162, 20)
        Me.txt_nome.TabIndex = 2
        '
        'cmb_rodadas
        '
        Me.cmb_rodadas.FormattingEnabled = True
        Me.cmb_rodadas.Location = New System.Drawing.Point(212, 75)
        Me.cmb_rodadas.Name = "cmb_rodadas"
        Me.cmb_rodadas.Size = New System.Drawing.Size(160, 21)
        Me.cmb_rodadas.TabIndex = 3
        '
        'grp_jogador
        '
        Me.grp_jogador.Controls.Add(Me.rdb_tesoura)
        Me.grp_jogador.Controls.Add(Me.img_tesoura)
        Me.grp_jogador.Controls.Add(Me.rdb_papel)
        Me.grp_jogador.Controls.Add(Me.img_papel)
        Me.grp_jogador.Controls.Add(Me.rdb_pedra)
        Me.grp_jogador.Controls.Add(Me.img_pedra)
        Me.grp_jogador.Location = New System.Drawing.Point(43, 143)
        Me.grp_jogador.Name = "grp_jogador"
        Me.grp_jogador.Size = New System.Drawing.Size(258, 116)
        Me.grp_jogador.TabIndex = 4
        Me.grp_jogador.TabStop = False
        Me.grp_jogador.Text = "ATAQUE DO JOGADOR"
        '
        'rdb_tesoura
        '
        Me.rdb_tesoura.AutoSize = True
        Me.rdb_tesoura.Location = New System.Drawing.Point(13, 72)
        Me.rdb_tesoura.Name = "rdb_tesoura"
        Me.rdb_tesoura.Size = New System.Drawing.Size(64, 17)
        Me.rdb_tesoura.TabIndex = 3
        Me.rdb_tesoura.TabStop = True
        Me.rdb_tesoura.Text = "Tesoura"
        Me.rdb_tesoura.UseVisualStyleBackColor = True
        '
        'img_tesoura
        '
        Me.img_tesoura.BackgroundImage = CType(resources.GetObject("img_tesoura.BackgroundImage"), System.Drawing.Image)
        Me.img_tesoura.Image = CType(resources.GetObject("img_tesoura.Image"), System.Drawing.Image)
        Me.img_tesoura.InitialImage = CType(resources.GetObject("img_tesoura.InitialImage"), System.Drawing.Image)
        Me.img_tesoura.Location = New System.Drawing.Point(137, 20)
        Me.img_tesoura.Name = "img_tesoura"
        Me.img_tesoura.Size = New System.Drawing.Size(99, 76)
        Me.img_tesoura.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_tesoura.TabIndex = 16
        Me.img_tesoura.TabStop = False
        '
        'rdb_papel
        '
        Me.rdb_papel.AutoSize = True
        Me.rdb_papel.Location = New System.Drawing.Point(13, 49)
        Me.rdb_papel.Name = "rdb_papel"
        Me.rdb_papel.Size = New System.Drawing.Size(52, 17)
        Me.rdb_papel.TabIndex = 2
        Me.rdb_papel.TabStop = True
        Me.rdb_papel.Text = "Papel"
        Me.rdb_papel.UseVisualStyleBackColor = True
        '
        'img_papel
        '
        Me.img_papel.BackgroundImage = CType(resources.GetObject("img_papel.BackgroundImage"), System.Drawing.Image)
        Me.img_papel.Image = CType(resources.GetObject("img_papel.Image"), System.Drawing.Image)
        Me.img_papel.Location = New System.Drawing.Point(115, 20)
        Me.img_papel.Name = "img_papel"
        Me.img_papel.Size = New System.Drawing.Size(99, 76)
        Me.img_papel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_papel.TabIndex = 15
        Me.img_papel.TabStop = False
        '
        'rdb_pedra
        '
        Me.rdb_pedra.AutoSize = True
        Me.rdb_pedra.Location = New System.Drawing.Point(13, 26)
        Me.rdb_pedra.Name = "rdb_pedra"
        Me.rdb_pedra.Size = New System.Drawing.Size(53, 17)
        Me.rdb_pedra.TabIndex = 1
        Me.rdb_pedra.TabStop = True
        Me.rdb_pedra.Text = "Pedra"
        Me.rdb_pedra.UseVisualStyleBackColor = True
        '
        'img_pedra
        '
        Me.img_pedra.BackgroundImage = CType(resources.GetObject("img_pedra.BackgroundImage"), System.Drawing.Image)
        Me.img_pedra.Image = CType(resources.GetObject("img_pedra.Image"), System.Drawing.Image)
        Me.img_pedra.Location = New System.Drawing.Point(98, 20)
        Me.img_pedra.Name = "img_pedra"
        Me.img_pedra.Size = New System.Drawing.Size(99, 76)
        Me.img_pedra.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_pedra.TabIndex = 0
        Me.img_pedra.TabStop = False
        '
        'grp_cpu
        '
        Me.grp_cpu.Controls.Add(Me.img_tesoura_cpu)
        Me.grp_cpu.Controls.Add(Me.img_papel_cpu)
        Me.grp_cpu.Controls.Add(Me.img_pedra_cpu)
        Me.grp_cpu.Location = New System.Drawing.Point(400, 143)
        Me.grp_cpu.Name = "grp_cpu"
        Me.grp_cpu.Size = New System.Drawing.Size(237, 116)
        Me.grp_cpu.TabIndex = 6
        Me.grp_cpu.TabStop = False
        Me.grp_cpu.Text = "ATAQUE DA CPU"
        '
        'img_tesoura_cpu
        '
        Me.img_tesoura_cpu.BackgroundImage = CType(resources.GetObject("img_tesoura_cpu.BackgroundImage"), System.Drawing.Image)
        Me.img_tesoura_cpu.Image = CType(resources.GetObject("img_tesoura_cpu.Image"), System.Drawing.Image)
        Me.img_tesoura_cpu.InitialImage = CType(resources.GetObject("img_tesoura_cpu.InitialImage"), System.Drawing.Image)
        Me.img_tesoura_cpu.Location = New System.Drawing.Point(54, 26)
        Me.img_tesoura_cpu.Name = "img_tesoura_cpu"
        Me.img_tesoura_cpu.Size = New System.Drawing.Size(99, 76)
        Me.img_tesoura_cpu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_tesoura_cpu.TabIndex = 19
        Me.img_tesoura_cpu.TabStop = False
        '
        'img_papel_cpu
        '
        Me.img_papel_cpu.BackgroundImage = CType(resources.GetObject("img_papel_cpu.BackgroundImage"), System.Drawing.Image)
        Me.img_papel_cpu.Image = CType(resources.GetObject("img_papel_cpu.Image"), System.Drawing.Image)
        Me.img_papel_cpu.Location = New System.Drawing.Point(30, 26)
        Me.img_papel_cpu.Name = "img_papel_cpu"
        Me.img_papel_cpu.Size = New System.Drawing.Size(99, 76)
        Me.img_papel_cpu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_papel_cpu.TabIndex = 20
        Me.img_papel_cpu.TabStop = False
        '
        'img_pedra_cpu
        '
        Me.img_pedra_cpu.BackgroundImage = CType(resources.GetObject("img_pedra_cpu.BackgroundImage"), System.Drawing.Image)
        Me.img_pedra_cpu.Image = CType(resources.GetObject("img_pedra_cpu.Image"), System.Drawing.Image)
        Me.img_pedra_cpu.Location = New System.Drawing.Point(6, 26)
        Me.img_pedra_cpu.Name = "img_pedra_cpu"
        Me.img_pedra_cpu.Size = New System.Drawing.Size(99, 76)
        Me.img_pedra_cpu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_pedra_cpu.TabIndex = 17
        Me.img_pedra_cpu.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(320, 176)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(61, 63)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 7
        Me.PictureBox2.TabStop = False
        '
        'btn_jogar
        '
        Me.btn_jogar.Location = New System.Drawing.Point(454, 32)
        Me.btn_jogar.Name = "btn_jogar"
        Me.btn_jogar.Size = New System.Drawing.Size(152, 78)
        Me.btn_jogar.TabIndex = 8
        Me.btn_jogar.Text = "JOGAR"
        Me.btn_jogar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.ControlLight
        Me.GroupBox2.Controls.Add(Me.lbl_rodada_atual)
        Me.GroupBox2.Controls.Add(Me.lbl_empate_texto)
        Me.GroupBox2.Controls.Add(Me.lbl_qtdd)
        Me.GroupBox2.Controls.Add(Me.lbl_empate)
        Me.GroupBox2.Controls.Add(Me.lbl_pc)
        Me.GroupBox2.Controls.Add(Me.lbl_cpu)
        Me.GroupBox2.Controls.Add(Me.lbl_jogador)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial Narrow", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(43, 278)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(236, 153)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Placar Geral"
        '
        'lbl_rodada_atual
        '
        Me.lbl_rodada_atual.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_rodada_atual.Location = New System.Drawing.Point(134, 101)
        Me.lbl_rodada_atual.Name = "lbl_rodada_atual"
        Me.lbl_rodada_atual.Size = New System.Drawing.Size(53, 16)
        Me.lbl_rodada_atual.TabIndex = 12
        '
        'lbl_empate_texto
        '
        Me.lbl_empate_texto.AutoSize = True
        Me.lbl_empate_texto.Location = New System.Drawing.Point(16, 72)
        Me.lbl_empate_texto.Name = "lbl_empate_texto"
        Me.lbl_empate_texto.Size = New System.Drawing.Size(38, 15)
        Me.lbl_empate_texto.TabIndex = 11
        Me.lbl_empate_texto.Text = "Empate"
        '
        'lbl_qtdd
        '
        Me.lbl_qtdd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_qtdd.Location = New System.Drawing.Point(134, 129)
        Me.lbl_qtdd.Name = "lbl_qtdd"
        Me.lbl_qtdd.Size = New System.Drawing.Size(53, 16)
        Me.lbl_qtdd.TabIndex = 10
        '
        'lbl_empate
        '
        Me.lbl_empate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_empate.Location = New System.Drawing.Point(134, 75)
        Me.lbl_empate.Name = "lbl_empate"
        Me.lbl_empate.Size = New System.Drawing.Size(53, 16)
        Me.lbl_empate.TabIndex = 9
        '
        'lbl_pc
        '
        Me.lbl_pc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_pc.Location = New System.Drawing.Point(134, 50)
        Me.lbl_pc.Name = "lbl_pc"
        Me.lbl_pc.Size = New System.Drawing.Size(53, 16)
        Me.lbl_pc.TabIndex = 8
        '
        'lbl_cpu
        '
        Me.lbl_cpu.AutoSize = True
        Me.lbl_cpu.Location = New System.Drawing.Point(134, 50)
        Me.lbl_cpu.Name = "lbl_cpu"
        Me.lbl_cpu.Size = New System.Drawing.Size(0, 15)
        Me.lbl_cpu.TabIndex = 5
        '
        'lbl_jogador
        '
        Me.lbl_jogador.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_jogador.Location = New System.Drawing.Point(134, 28)
        Me.lbl_jogador.Name = "lbl_jogador"
        Me.lbl_jogador.Size = New System.Drawing.Size(53, 16)
        Me.lbl_jogador.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(16, 129)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 15)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Qtdd de Rodadas:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 15)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Rodada Atual:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 50)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 15)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "CPU"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(16, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 15)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Jogador:"
        '
        'Label12
        '
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Red
        Me.Label12.Location = New System.Drawing.Point(265, -1)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(152, 37)
        Me.Label12.TabIndex = 14
        Me.Label12.Text = "JOKENPO"
        '
        'lbl_mensagem
        '
        Me.lbl_mensagem.BackColor = System.Drawing.Color.White
        Me.lbl_mensagem.Location = New System.Drawing.Point(318, 262)
        Me.lbl_mensagem.Name = "lbl_mensagem"
        Me.lbl_mensagem.Size = New System.Drawing.Size(319, 24)
        Me.lbl_mensagem.TabIndex = 21
        '
        'btn_jogar_novamente
        '
        Me.btn_jogar_novamente.Location = New System.Drawing.Point(320, 289)
        Me.btn_jogar_novamente.Name = "btn_jogar_novamente"
        Me.btn_jogar_novamente.Size = New System.Drawing.Size(152, 78)
        Me.btn_jogar_novamente.TabIndex = 22
        Me.btn_jogar_novamente.Text = "Jogar novamente"
        Me.btn_jogar_novamente.UseVisualStyleBackColor = True
        '
        'frm_jokenpo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(694, 455)
        Me.Controls.Add(Me.btn_jogar_novamente)
        Me.Controls.Add(Me.lbl_mensagem)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btn_jogar)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.grp_cpu)
        Me.Controls.Add(Me.grp_jogador)
        Me.Controls.Add(Me.cmb_rodadas)
        Me.Controls.Add(Me.txt_nome)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm_jokenpo"
        Me.Text = "jokenpo"
        Me.grp_jogador.ResumeLayout(False)
        Me.grp_jogador.PerformLayout()
        CType(Me.img_tesoura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_papel, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_pedra, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grp_cpu.ResumeLayout(False)
        CType(Me.img_tesoura_cpu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_papel_cpu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_pedra_cpu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_nome As TextBox
    Friend WithEvents cmb_rodadas As ComboBox
    Friend WithEvents grp_jogador As GroupBox
    Friend WithEvents rdb_tesoura As RadioButton
    Friend WithEvents rdb_papel As RadioButton
    Friend WithEvents rdb_pedra As RadioButton
    Friend WithEvents img_pedra As PictureBox
    Friend WithEvents grp_cpu As GroupBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents btn_jogar As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents lbl_rodada_atual As Label
    Friend WithEvents lbl_empate_texto As Label
    Friend WithEvents lbl_qtdd As Label
    Friend WithEvents lbl_empate As Label
    Friend WithEvents lbl_pc As Label
    Friend WithEvents lbl_cpu As Label
    Friend WithEvents lbl_jogador As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents img_papel As PictureBox
    Friend WithEvents img_tesoura As PictureBox
    Friend WithEvents img_pedra_cpu As PictureBox
    Friend WithEvents img_tesoura_cpu As PictureBox
    Friend WithEvents img_papel_cpu As PictureBox
    Friend WithEvents lbl_mensagem As Label
    Friend WithEvents btn_jogar_novamente As Button
End Class
