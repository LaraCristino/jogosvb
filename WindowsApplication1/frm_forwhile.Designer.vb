﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_forwhile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_ni = New System.Windows.Forms.Label()
        Me.lbl_final = New System.Windows.Forms.Label()
        Me.lbl_inc = New System.Windows.Forms.Label()
        Me.txt_ni = New System.Windows.Forms.TextBox()
        Me.txt_nf = New System.Windows.Forms.TextBox()
        Me.txt_inc = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.cmb_sentido = New System.Windows.Forms.ComboBox()
        Me.rdb_while = New System.Windows.Forms.RadioButton()
        Me.rdb_for = New System.Windows.Forms.RadioButton()
        Me.lst_numeros_gerados = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbl_tipo = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_ni
        '
        Me.lbl_ni.AutoSize = True
        Me.lbl_ni.Location = New System.Drawing.Point(44, 31)
        Me.lbl_ni.Name = "lbl_ni"
        Me.lbl_ni.Size = New System.Drawing.Size(74, 13)
        Me.lbl_ni.TabIndex = 0
        Me.lbl_ni.Text = "Número Inicial"
        '
        'lbl_final
        '
        Me.lbl_final.AutoSize = True
        Me.lbl_final.Location = New System.Drawing.Point(44, 59)
        Me.lbl_final.Name = "lbl_final"
        Me.lbl_final.Size = New System.Drawing.Size(69, 13)
        Me.lbl_final.TabIndex = 1
        Me.lbl_final.Text = "Número Final"
        '
        'lbl_inc
        '
        Me.lbl_inc.AutoSize = True
        Me.lbl_inc.Location = New System.Drawing.Point(44, 88)
        Me.lbl_inc.Name = "lbl_inc"
        Me.lbl_inc.Size = New System.Drawing.Size(60, 13)
        Me.lbl_inc.TabIndex = 2
        Me.lbl_inc.Text = "Incremento"
        '
        'txt_ni
        '
        Me.txt_ni.Location = New System.Drawing.Point(165, 24)
        Me.txt_ni.Name = "txt_ni"
        Me.txt_ni.Size = New System.Drawing.Size(100, 20)
        Me.txt_ni.TabIndex = 3
        '
        'txt_nf
        '
        Me.txt_nf.Location = New System.Drawing.Point(165, 56)
        Me.txt_nf.Name = "txt_nf"
        Me.txt_nf.Size = New System.Drawing.Size(100, 20)
        Me.txt_nf.TabIndex = 4
        '
        'txt_inc
        '
        Me.txt_inc.Location = New System.Drawing.Point(165, 88)
        Me.txt_inc.Name = "txt_inc"
        Me.txt_inc.Size = New System.Drawing.Size(100, 20)
        Me.txt_inc.TabIndex = 5
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btn_ok)
        Me.GroupBox1.Controls.Add(Me.cmb_sentido)
        Me.GroupBox1.Controls.Add(Me.rdb_while)
        Me.GroupBox1.Controls.Add(Me.rdb_for)
        Me.GroupBox1.Location = New System.Drawing.Point(47, 140)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(217, 174)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Selecione"
        '
        'btn_ok
        '
        Me.btn_ok.Location = New System.Drawing.Point(143, 49)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(59, 23)
        Me.btn_ok.TabIndex = 3
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'cmb_sentido
        '
        Me.cmb_sentido.FormattingEnabled = True
        Me.cmb_sentido.Location = New System.Drawing.Point(6, 122)
        Me.cmb_sentido.Name = "cmb_sentido"
        Me.cmb_sentido.Size = New System.Drawing.Size(205, 21)
        Me.cmb_sentido.TabIndex = 2
        '
        'rdb_while
        '
        Me.rdb_while.AutoSize = True
        Me.rdb_while.Location = New System.Drawing.Point(18, 65)
        Me.rdb_while.Name = "rdb_while"
        Me.rdb_while.Size = New System.Drawing.Size(52, 17)
        Me.rdb_while.TabIndex = 1
        Me.rdb_while.TabStop = True
        Me.rdb_while.Text = "While"
        Me.rdb_while.UseVisualStyleBackColor = True
        '
        'rdb_for
        '
        Me.rdb_for.AutoSize = True
        Me.rdb_for.Location = New System.Drawing.Point(18, 33)
        Me.rdb_for.Name = "rdb_for"
        Me.rdb_for.Size = New System.Drawing.Size(40, 17)
        Me.rdb_for.TabIndex = 0
        Me.rdb_for.TabStop = True
        Me.rdb_for.Text = "For"
        Me.rdb_for.UseVisualStyleBackColor = True
        '
        'lst_numeros_gerados
        '
        Me.lst_numeros_gerados.FormattingEnabled = True
        Me.lst_numeros_gerados.Location = New System.Drawing.Point(356, 91)
        Me.lst_numeros_gerados.Name = "lst_numeros_gerados"
        Me.lst_numeros_gerados.Size = New System.Drawing.Size(207, 225)
        Me.lst_numeros_gerados.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(353, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Números Gerados:"
        '
        'lbl_tipo
        '
        Me.lbl_tipo.AutoSize = True
        Me.lbl_tipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_tipo.ForeColor = System.Drawing.SystemColors.ControlLight
        Me.lbl_tipo.Location = New System.Drawing.Point(505, 54)
        Me.lbl_tipo.Name = "lbl_tipo"
        Me.lbl_tipo.Size = New System.Drawing.Size(2, 15)
        Me.lbl_tipo.TabIndex = 9
        '
        'frm_forwhile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(606, 348)
        Me.Controls.Add(Me.lbl_tipo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lst_numeros_gerados)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txt_inc)
        Me.Controls.Add(Me.txt_nf)
        Me.Controls.Add(Me.txt_ni)
        Me.Controls.Add(Me.lbl_inc)
        Me.Controls.Add(Me.lbl_final)
        Me.Controls.Add(Me.lbl_ni)
        Me.Name = "frm_forwhile"
        Me.Text = "frm_forwhile"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_ni As Label
    Friend WithEvents lbl_final As Label
    Friend WithEvents lbl_inc As Label
    Friend WithEvents txt_ni As TextBox
    Friend WithEvents txt_nf As TextBox
    Friend WithEvents txt_inc As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btn_ok As Button
    Friend WithEvents cmb_sentido As ComboBox
    Friend WithEvents rdb_while As RadioButton
    Friend WithEvents rdb_for As RadioButton
    Friend WithEvents lst_numeros_gerados As ListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lbl_tipo As Label
End Class
