﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_jogodaforca
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm_jogodaforca))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.grp_letras = New System.Windows.Forms.GroupBox()
        Me.btn_r = New System.Windows.Forms.Button()
        Me.btn_i = New System.Windows.Forms.Button()
        Me.btn_q = New System.Windows.Forms.Button()
        Me.btn_z = New System.Windows.Forms.Button()
        Me.btn_h = New System.Windows.Forms.Button()
        Me.btn_p = New System.Windows.Forms.Button()
        Me.btn_x = New System.Windows.Forms.Button()
        Me.btn_g = New System.Windows.Forms.Button()
        Me.btn_o = New System.Windows.Forms.Button()
        Me.btn_y = New System.Windows.Forms.Button()
        Me.btn_f = New System.Windows.Forms.Button()
        Me.btn_n = New System.Windows.Forms.Button()
        Me.btn_w = New System.Windows.Forms.Button()
        Me.btn_e = New System.Windows.Forms.Button()
        Me.btn_m = New System.Windows.Forms.Button()
        Me.btn_v = New System.Windows.Forms.Button()
        Me.btn_d = New System.Windows.Forms.Button()
        Me.btn_l = New System.Windows.Forms.Button()
        Me.btn_u = New System.Windows.Forms.Button()
        Me.btn_c = New System.Windows.Forms.Button()
        Me.btn_k = New System.Windows.Forms.Button()
        Me.btn_t = New System.Windows.Forms.Button()
        Me.btn_b = New System.Windows.Forms.Button()
        Me.btn_j = New System.Windows.Forms.Button()
        Me.btn_s = New System.Windows.Forms.Button()
        Me.btn_a = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txt_dica = New System.Windows.Forms.TextBox()
        Me.btn_jogar = New System.Windows.Forms.Button()
        Me.lbl_palavra = New System.Windows.Forms.Label()
        Me.img_cabeca = New System.Windows.Forms.PictureBox()
        Me.img_tronco = New System.Windows.Forms.PictureBox()
        Me.img_braco1 = New System.Windows.Forms.PictureBox()
        Me.img_braco2 = New System.Windows.Forms.PictureBox()
        Me.img_perna1 = New System.Windows.Forms.PictureBox()
        Me.img_perna2 = New System.Windows.Forms.PictureBox()
        Me.LetrasDigitadas = New System.Windows.Forms.ListBox()
        Me.lbl_invalido = New System.Windows.Forms.Label()
        Me.txt_palavra = New System.Windows.Forms.TextBox()
        Me.lbl_mensagem = New System.Windows.Forms.Label()
        Me.btn_jogar_novamente = New System.Windows.Forms.Button()
        Me.lbl_letras_digitadas = New System.Windows.Forms.Label()
        Me.grp_letras.SuspendLayout()
        CType(Me.img_cabeca, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_tronco, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_braco1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_braco2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_perna1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.img_perna2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Maiandra GD", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Image = CType(resources.GetObject("Label1.Image"), System.Drawing.Image)
        Me.Label1.Location = New System.Drawing.Point(298, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(267, 46)
        Me.Label1.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.Image = CType(resources.GetObject("Label2.Image"), System.Drawing.Image)
        Me.Label2.Location = New System.Drawing.Point(6, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(188, 326)
        Me.Label2.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Image = CType(resources.GetObject("Label3.Image"), System.Drawing.Image)
        Me.Label3.Location = New System.Drawing.Point(-2, -13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(890, 539)
        Me.Label3.TabIndex = 7
        '
        'grp_letras
        '
        Me.grp_letras.BackColor = System.Drawing.Color.DimGray
        Me.grp_letras.Controls.Add(Me.btn_r)
        Me.grp_letras.Controls.Add(Me.btn_i)
        Me.grp_letras.Controls.Add(Me.btn_q)
        Me.grp_letras.Controls.Add(Me.btn_z)
        Me.grp_letras.Controls.Add(Me.btn_h)
        Me.grp_letras.Controls.Add(Me.btn_p)
        Me.grp_letras.Controls.Add(Me.btn_x)
        Me.grp_letras.Controls.Add(Me.btn_g)
        Me.grp_letras.Controls.Add(Me.btn_o)
        Me.grp_letras.Controls.Add(Me.btn_y)
        Me.grp_letras.Controls.Add(Me.btn_f)
        Me.grp_letras.Controls.Add(Me.btn_n)
        Me.grp_letras.Controls.Add(Me.btn_w)
        Me.grp_letras.Controls.Add(Me.btn_e)
        Me.grp_letras.Controls.Add(Me.btn_m)
        Me.grp_letras.Controls.Add(Me.btn_v)
        Me.grp_letras.Controls.Add(Me.btn_d)
        Me.grp_letras.Controls.Add(Me.btn_l)
        Me.grp_letras.Controls.Add(Me.btn_u)
        Me.grp_letras.Controls.Add(Me.btn_c)
        Me.grp_letras.Controls.Add(Me.btn_k)
        Me.grp_letras.Controls.Add(Me.btn_t)
        Me.grp_letras.Controls.Add(Me.btn_b)
        Me.grp_letras.Controls.Add(Me.btn_j)
        Me.grp_letras.Controls.Add(Me.btn_s)
        Me.grp_letras.Controls.Add(Me.btn_a)
        Me.grp_letras.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_letras.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.grp_letras.Location = New System.Drawing.Point(100, 408)
        Me.grp_letras.Name = "grp_letras"
        Me.grp_letras.Size = New System.Drawing.Size(613, 87)
        Me.grp_letras.TabIndex = 8
        Me.grp_letras.TabStop = False
        Me.grp_letras.Text = "Alfabeto"
        '
        'btn_r
        '
        Me.btn_r.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_r.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_r.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_r.Location = New System.Drawing.Point(200, 56)
        Me.btn_r.Name = "btn_r"
        Me.btn_r.Size = New System.Drawing.Size(38, 25)
        Me.btn_r.TabIndex = 30
        Me.btn_r.Text = "R"
        Me.btn_r.UseVisualStyleBackColor = False
        '
        'btn_i
        '
        Me.btn_i.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_i.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_i.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_i.Location = New System.Drawing.Point(378, 23)
        Me.btn_i.Name = "btn_i"
        Me.btn_i.Size = New System.Drawing.Size(38, 25)
        Me.btn_i.TabIndex = 28
        Me.btn_i.Text = "I"
        Me.btn_i.UseVisualStyleBackColor = False
        '
        'btn_q
        '
        Me.btn_q.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_q.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_q.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_q.Location = New System.Drawing.Point(156, 56)
        Me.btn_q.Name = "btn_q"
        Me.btn_q.Size = New System.Drawing.Size(38, 25)
        Me.btn_q.TabIndex = 27
        Me.btn_q.Text = "Q"
        Me.btn_q.UseVisualStyleBackColor = False
        '
        'btn_z
        '
        Me.btn_z.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_z.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_z.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_z.Location = New System.Drawing.Point(554, 56)
        Me.btn_z.Name = "btn_z"
        Me.btn_z.Size = New System.Drawing.Size(38, 25)
        Me.btn_z.TabIndex = 26
        Me.btn_z.Text = "Z"
        Me.btn_z.UseVisualStyleBackColor = False
        '
        'btn_h
        '
        Me.btn_h.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_h.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_h.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_h.Location = New System.Drawing.Point(334, 23)
        Me.btn_h.Name = "btn_h"
        Me.btn_h.Size = New System.Drawing.Size(38, 25)
        Me.btn_h.TabIndex = 25
        Me.btn_h.Text = "H"
        Me.btn_h.UseVisualStyleBackColor = False
        '
        'btn_p
        '
        Me.btn_p.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_p.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_p.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_p.Location = New System.Drawing.Point(112, 56)
        Me.btn_p.Name = "btn_p"
        Me.btn_p.Size = New System.Drawing.Size(38, 25)
        Me.btn_p.TabIndex = 24
        Me.btn_p.Text = "P"
        Me.btn_p.UseVisualStyleBackColor = False
        '
        'btn_x
        '
        Me.btn_x.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_x.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_x.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_x.Location = New System.Drawing.Point(466, 56)
        Me.btn_x.Name = "btn_x"
        Me.btn_x.Size = New System.Drawing.Size(38, 25)
        Me.btn_x.TabIndex = 23
        Me.btn_x.Text = "X"
        Me.btn_x.UseVisualStyleBackColor = False
        '
        'btn_g
        '
        Me.btn_g.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_g.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_g.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_g.Location = New System.Drawing.Point(290, 23)
        Me.btn_g.Name = "btn_g"
        Me.btn_g.Size = New System.Drawing.Size(38, 25)
        Me.btn_g.TabIndex = 22
        Me.btn_g.Text = "G"
        Me.btn_g.UseVisualStyleBackColor = False
        '
        'btn_o
        '
        Me.btn_o.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_o.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_o.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_o.Location = New System.Drawing.Point(68, 56)
        Me.btn_o.Name = "btn_o"
        Me.btn_o.Size = New System.Drawing.Size(38, 25)
        Me.btn_o.TabIndex = 21
        Me.btn_o.Text = "O"
        Me.btn_o.UseVisualStyleBackColor = False
        '
        'btn_y
        '
        Me.btn_y.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_y.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_y.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_y.Location = New System.Drawing.Point(510, 56)
        Me.btn_y.Name = "btn_y"
        Me.btn_y.Size = New System.Drawing.Size(38, 25)
        Me.btn_y.TabIndex = 20
        Me.btn_y.Text = "Y"
        Me.btn_y.UseVisualStyleBackColor = False
        '
        'btn_f
        '
        Me.btn_f.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_f.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_f.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_f.Location = New System.Drawing.Point(244, 23)
        Me.btn_f.Name = "btn_f"
        Me.btn_f.Size = New System.Drawing.Size(38, 25)
        Me.btn_f.TabIndex = 19
        Me.btn_f.Text = "F"
        Me.btn_f.UseVisualStyleBackColor = False
        '
        'btn_n
        '
        Me.btn_n.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_n.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_n.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_n.Location = New System.Drawing.Point(24, 56)
        Me.btn_n.Name = "btn_n"
        Me.btn_n.Size = New System.Drawing.Size(38, 25)
        Me.btn_n.TabIndex = 18
        Me.btn_n.Text = "N"
        Me.btn_n.UseVisualStyleBackColor = False
        '
        'btn_w
        '
        Me.btn_w.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_w.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_w.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_w.Location = New System.Drawing.Point(422, 56)
        Me.btn_w.Name = "btn_w"
        Me.btn_w.Size = New System.Drawing.Size(38, 25)
        Me.btn_w.TabIndex = 17
        Me.btn_w.Text = "W"
        Me.btn_w.UseVisualStyleBackColor = False
        '
        'btn_e
        '
        Me.btn_e.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_e.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_e.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_e.Location = New System.Drawing.Point(200, 23)
        Me.btn_e.Name = "btn_e"
        Me.btn_e.Size = New System.Drawing.Size(38, 25)
        Me.btn_e.TabIndex = 16
        Me.btn_e.Text = "E"
        Me.btn_e.UseVisualStyleBackColor = False
        '
        'btn_m
        '
        Me.btn_m.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_m.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_m.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_m.Location = New System.Drawing.Point(554, 23)
        Me.btn_m.Name = "btn_m"
        Me.btn_m.Size = New System.Drawing.Size(38, 25)
        Me.btn_m.TabIndex = 15
        Me.btn_m.Text = "M"
        Me.btn_m.UseVisualStyleBackColor = False
        '
        'btn_v
        '
        Me.btn_v.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_v.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_v.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_v.Location = New System.Drawing.Point(378, 56)
        Me.btn_v.Name = "btn_v"
        Me.btn_v.Size = New System.Drawing.Size(38, 25)
        Me.btn_v.TabIndex = 14
        Me.btn_v.Text = "V"
        Me.btn_v.UseVisualStyleBackColor = False
        '
        'btn_d
        '
        Me.btn_d.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_d.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_d.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_d.Location = New System.Drawing.Point(156, 23)
        Me.btn_d.Name = "btn_d"
        Me.btn_d.Size = New System.Drawing.Size(38, 25)
        Me.btn_d.TabIndex = 13
        Me.btn_d.Text = "D"
        Me.btn_d.UseVisualStyleBackColor = False
        '
        'btn_l
        '
        Me.btn_l.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_l.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_l.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_l.Location = New System.Drawing.Point(510, 23)
        Me.btn_l.Name = "btn_l"
        Me.btn_l.Size = New System.Drawing.Size(38, 25)
        Me.btn_l.TabIndex = 12
        Me.btn_l.Text = "L"
        Me.btn_l.UseVisualStyleBackColor = False
        '
        'btn_u
        '
        Me.btn_u.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_u.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_u.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_u.Location = New System.Drawing.Point(334, 56)
        Me.btn_u.Name = "btn_u"
        Me.btn_u.Size = New System.Drawing.Size(38, 25)
        Me.btn_u.TabIndex = 11
        Me.btn_u.Text = "U"
        Me.btn_u.UseVisualStyleBackColor = False
        '
        'btn_c
        '
        Me.btn_c.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_c.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_c.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_c.Location = New System.Drawing.Point(111, 23)
        Me.btn_c.Name = "btn_c"
        Me.btn_c.Size = New System.Drawing.Size(38, 25)
        Me.btn_c.TabIndex = 10
        Me.btn_c.Text = "C"
        Me.btn_c.UseVisualStyleBackColor = False
        '
        'btn_k
        '
        Me.btn_k.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_k.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_k.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_k.Location = New System.Drawing.Point(466, 23)
        Me.btn_k.Name = "btn_k"
        Me.btn_k.Size = New System.Drawing.Size(38, 25)
        Me.btn_k.TabIndex = 9
        Me.btn_k.Text = "K"
        Me.btn_k.UseVisualStyleBackColor = False
        '
        'btn_t
        '
        Me.btn_t.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_t.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_t.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_t.Location = New System.Drawing.Point(290, 56)
        Me.btn_t.Name = "btn_t"
        Me.btn_t.Size = New System.Drawing.Size(38, 25)
        Me.btn_t.TabIndex = 8
        Me.btn_t.Text = "T"
        Me.btn_t.UseVisualStyleBackColor = False
        '
        'btn_b
        '
        Me.btn_b.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_b.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_b.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_b.Location = New System.Drawing.Point(68, 23)
        Me.btn_b.Name = "btn_b"
        Me.btn_b.Size = New System.Drawing.Size(38, 25)
        Me.btn_b.TabIndex = 7
        Me.btn_b.Text = "B"
        Me.btn_b.UseVisualStyleBackColor = False
        '
        'btn_j
        '
        Me.btn_j.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_j.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_j.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_j.Location = New System.Drawing.Point(422, 23)
        Me.btn_j.Name = "btn_j"
        Me.btn_j.Size = New System.Drawing.Size(38, 25)
        Me.btn_j.TabIndex = 6
        Me.btn_j.Text = "J"
        Me.btn_j.UseVisualStyleBackColor = False
        '
        'btn_s
        '
        Me.btn_s.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_s.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_s.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_s.Location = New System.Drawing.Point(244, 56)
        Me.btn_s.Name = "btn_s"
        Me.btn_s.Size = New System.Drawing.Size(38, 25)
        Me.btn_s.TabIndex = 5
        Me.btn_s.Text = "S"
        Me.btn_s.UseVisualStyleBackColor = False
        '
        'btn_a
        '
        Me.btn_a.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.btn_a.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_a.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btn_a.Location = New System.Drawing.Point(24, 23)
        Me.btn_a.Name = "btn_a"
        Me.btn_a.Size = New System.Drawing.Size(38, 25)
        Me.btn_a.TabIndex = 0
        Me.btn_a.Text = "A"
        Me.btn_a.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Image = CType(resources.GetObject("Label4.Image"), System.Drawing.Image)
        Me.Label4.Location = New System.Drawing.Point(192, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(164, 33)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Digite a palavra:"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Image = CType(resources.GetObject("Label5.Image"), System.Drawing.Image)
        Me.Label5.Location = New System.Drawing.Point(192, 107)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(164, 33)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Digite a dica:"
        '
        'txt_dica
        '
        Me.txt_dica.Location = New System.Drawing.Point(333, 110)
        Me.txt_dica.Multiline = True
        Me.txt_dica.Name = "txt_dica"
        Me.txt_dica.Size = New System.Drawing.Size(183, 30)
        Me.txt_dica.TabIndex = 12
        '
        'btn_jogar
        '
        Me.btn_jogar.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btn_jogar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_jogar.Location = New System.Drawing.Point(542, 79)
        Me.btn_jogar.Name = "btn_jogar"
        Me.btn_jogar.Size = New System.Drawing.Size(126, 61)
        Me.btn_jogar.TabIndex = 13
        Me.btn_jogar.Text = "JOGAR!"
        Me.btn_jogar.UseVisualStyleBackColor = False
        '
        'lbl_palavra
        '
        Me.lbl_palavra.AutoSize = True
        Me.lbl_palavra.Font = New System.Drawing.Font("Goudy Stout", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_palavra.Location = New System.Drawing.Point(126, 365)
        Me.lbl_palavra.Name = "lbl_palavra"
        Me.lbl_palavra.Size = New System.Drawing.Size(279, 40)
        Me.lbl_palavra.TabIndex = 14
        Me.lbl_palavra.Text = "PALAVRA"
        '
        'img_cabeca
        '
        Me.img_cabeca.BackColor = System.Drawing.Color.Transparent
        Me.img_cabeca.BackgroundImage = CType(resources.GetObject("img_cabeca.BackgroundImage"), System.Drawing.Image)
        Me.img_cabeca.Cursor = System.Windows.Forms.Cursors.No
        Me.img_cabeca.ErrorImage = CType(resources.GetObject("img_cabeca.ErrorImage"), System.Drawing.Image)
        Me.img_cabeca.Location = New System.Drawing.Point(95, 186)
        Me.img_cabeca.Name = "img_cabeca"
        Me.img_cabeca.Size = New System.Drawing.Size(64, 60)
        Me.img_cabeca.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.img_cabeca.TabIndex = 16
        Me.img_cabeca.TabStop = False
        '
        'img_tronco
        '
        Me.img_tronco.BackgroundImage = CType(resources.GetObject("img_tronco.BackgroundImage"), System.Drawing.Image)
        Me.img_tronco.Location = New System.Drawing.Point(123, 247)
        Me.img_tronco.Name = "img_tronco"
        Me.img_tronco.Size = New System.Drawing.Size(10, 82)
        Me.img_tronco.TabIndex = 17
        Me.img_tronco.TabStop = False
        '
        'img_braco1
        '
        Me.img_braco1.BackgroundImage = CType(resources.GetObject("img_braco1.BackgroundImage"), System.Drawing.Image)
        Me.img_braco1.Location = New System.Drawing.Point(86, 247)
        Me.img_braco1.Name = "img_braco1"
        Me.img_braco1.Size = New System.Drawing.Size(38, 28)
        Me.img_braco1.TabIndex = 18
        Me.img_braco1.TabStop = False
        '
        'img_braco2
        '
        Me.img_braco2.BackgroundImage = CType(resources.GetObject("img_braco2.BackgroundImage"), System.Drawing.Image)
        Me.img_braco2.Location = New System.Drawing.Point(133, 246)
        Me.img_braco2.Name = "img_braco2"
        Me.img_braco2.Size = New System.Drawing.Size(44, 28)
        Me.img_braco2.TabIndex = 19
        Me.img_braco2.TabStop = False
        '
        'img_perna1
        '
        Me.img_perna1.BackgroundImage = CType(resources.GetObject("img_perna1.BackgroundImage"), System.Drawing.Image)
        Me.img_perna1.Location = New System.Drawing.Point(90, 308)
        Me.img_perna1.Name = "img_perna1"
        Me.img_perna1.Size = New System.Drawing.Size(34, 40)
        Me.img_perna1.TabIndex = 20
        Me.img_perna1.TabStop = False
        '
        'img_perna2
        '
        Me.img_perna2.BackgroundImage = CType(resources.GetObject("img_perna2.BackgroundImage"), System.Drawing.Image)
        Me.img_perna2.Location = New System.Drawing.Point(129, 309)
        Me.img_perna2.Name = "img_perna2"
        Me.img_perna2.Size = New System.Drawing.Size(26, 40)
        Me.img_perna2.TabIndex = 21
        Me.img_perna2.TabStop = False
        '
        'LetrasDigitadas
        '
        Me.LetrasDigitadas.FormattingEnabled = True
        Me.LetrasDigitadas.Location = New System.Drawing.Point(603, 179)
        Me.LetrasDigitadas.Name = "LetrasDigitadas"
        Me.LetrasDigitadas.Size = New System.Drawing.Size(121, 173)
        Me.LetrasDigitadas.TabIndex = 22
        '
        'lbl_invalido
        '
        Me.lbl_invalido.AutoSize = True
        Me.lbl_invalido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_invalido.Location = New System.Drawing.Point(235, 59)
        Me.lbl_invalido.Name = "lbl_invalido"
        Me.lbl_invalido.Size = New System.Drawing.Size(15, 15)
        Me.lbl_invalido.TabIndex = 23
        Me.lbl_invalido.Text = ".."
        '
        'txt_palavra
        '
        Me.txt_palavra.Location = New System.Drawing.Point(333, 78)
        Me.txt_palavra.MaxLength = 10
        Me.txt_palavra.Multiline = True
        Me.txt_palavra.Name = "txt_palavra"
        Me.txt_palavra.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txt_palavra.Size = New System.Drawing.Size(183, 28)
        Me.txt_palavra.TabIndex = 24
        '
        'lbl_mensagem
        '
        Me.lbl_mensagem.AutoSize = True
        Me.lbl_mensagem.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_mensagem.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_mensagem.ForeColor = System.Drawing.Color.Red
        Me.lbl_mensagem.Location = New System.Drawing.Point(333, 186)
        Me.lbl_mensagem.Name = "lbl_mensagem"
        Me.lbl_mensagem.Size = New System.Drawing.Size(20, 20)
        Me.lbl_mensagem.TabIndex = 25
        Me.lbl_mensagem.Text = ".."
        '
        'btn_jogar_novamente
        '
        Me.btn_jogar_novamente.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btn_jogar_novamente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_jogar_novamente.Location = New System.Drawing.Point(333, 225)
        Me.btn_jogar_novamente.Name = "btn_jogar_novamente"
        Me.btn_jogar_novamente.Size = New System.Drawing.Size(126, 61)
        Me.btn_jogar_novamente.TabIndex = 26
        Me.btn_jogar_novamente.Text = "Jogar novamente"
        Me.btn_jogar_novamente.UseVisualStyleBackColor = False
        '
        'lbl_letras_digitadas
        '
        Me.lbl_letras_digitadas.AutoSize = True
        Me.lbl_letras_digitadas.BackColor = System.Drawing.SystemColors.Window
        Me.lbl_letras_digitadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl_letras_digitadas.Location = New System.Drawing.Point(600, 161)
        Me.lbl_letras_digitadas.Name = "lbl_letras_digitadas"
        Me.lbl_letras_digitadas.Size = New System.Drawing.Size(124, 15)
        Me.lbl_letras_digitadas.TabIndex = 27
        Me.lbl_letras_digitadas.Text = "Letras digitadas erradas:"
        '
        'frm_jogodaforca
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(734, 517)
        Me.Controls.Add(Me.lbl_letras_digitadas)
        Me.Controls.Add(Me.btn_jogar_novamente)
        Me.Controls.Add(Me.lbl_mensagem)
        Me.Controls.Add(Me.txt_palavra)
        Me.Controls.Add(Me.lbl_invalido)
        Me.Controls.Add(Me.LetrasDigitadas)
        Me.Controls.Add(Me.img_perna2)
        Me.Controls.Add(Me.img_perna1)
        Me.Controls.Add(Me.img_braco2)
        Me.Controls.Add(Me.img_braco1)
        Me.Controls.Add(Me.img_tronco)
        Me.Controls.Add(Me.img_cabeca)
        Me.Controls.Add(Me.lbl_palavra)
        Me.Controls.Add(Me.btn_jogar)
        Me.Controls.Add(Me.txt_dica)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.grp_letras)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Name = "frm_jogodaforca"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.Text = "frm_jogodaforca"
        Me.grp_letras.ResumeLayout(False)
        CType(Me.img_cabeca, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_tronco, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_braco1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_braco2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_perna1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.img_perna2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grp_letras As System.Windows.Forms.GroupBox
    Friend WithEvents btn_r As System.Windows.Forms.Button
    Friend WithEvents btn_i As System.Windows.Forms.Button
    Friend WithEvents btn_q As System.Windows.Forms.Button
    Friend WithEvents btn_z As System.Windows.Forms.Button
    Friend WithEvents btn_h As System.Windows.Forms.Button
    Friend WithEvents btn_p As System.Windows.Forms.Button
    Friend WithEvents btn_x As System.Windows.Forms.Button
    Friend WithEvents btn_g As System.Windows.Forms.Button
    Friend WithEvents btn_o As System.Windows.Forms.Button
    Friend WithEvents btn_y As System.Windows.Forms.Button
    Friend WithEvents btn_f As System.Windows.Forms.Button
    Friend WithEvents btn_n As System.Windows.Forms.Button
    Friend WithEvents btn_w As System.Windows.Forms.Button
    Friend WithEvents btn_e As System.Windows.Forms.Button
    Friend WithEvents btn_m As System.Windows.Forms.Button
    Friend WithEvents btn_v As System.Windows.Forms.Button
    Friend WithEvents btn_d As System.Windows.Forms.Button
    Friend WithEvents btn_l As System.Windows.Forms.Button
    Friend WithEvents btn_u As System.Windows.Forms.Button
    Friend WithEvents btn_c As System.Windows.Forms.Button
    Friend WithEvents btn_k As System.Windows.Forms.Button
    Friend WithEvents btn_t As System.Windows.Forms.Button
    Friend WithEvents btn_b As System.Windows.Forms.Button
    Friend WithEvents btn_j As System.Windows.Forms.Button
    Friend WithEvents btn_s As System.Windows.Forms.Button
    Friend WithEvents btn_a As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txt_dica As System.Windows.Forms.TextBox
    Friend WithEvents btn_jogar As Button
    Friend WithEvents lbl_palavra As Label
    Friend WithEvents img_cabeca As PictureBox
    Friend WithEvents img_tronco As PictureBox
    Friend WithEvents img_braco1 As PictureBox
    Friend WithEvents img_braco2 As PictureBox
    Friend WithEvents img_perna1 As PictureBox
    Friend WithEvents img_perna2 As PictureBox
    Friend WithEvents LetrasDigitadas As ListBox
    Friend WithEvents lbl_invalido As Label
    Friend WithEvents txt_palavra As TextBox
    Friend WithEvents lbl_mensagem As Label
    Friend WithEvents btn_jogar_novamente As Button
    Friend WithEvents lbl_letras_digitadas As Label
End Class
