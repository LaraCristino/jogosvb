﻿Public Class frm_forwhile

    Private Sub iterateFor(inicial As Integer, final As Integer, incremento As Integer, sentido As String)
        If (LCase(sentido) = "crescente") Then
            For i As Integer = inicial To final Step incremento
                lst_numeros_gerados.Items.Add(i)
            Next
        ElseIf (LCase(sentido) = "decrescente") Then
            For i As Integer = final To inicial Step -incremento
                lst_numeros_gerados.Items.Add(i)
            Next
        End If
    End Sub

    Private Sub iterateWhile(inicial As Integer, final As Integer, incremento As Integer, sentido As String)
        Dim i As Integer = 0
        Dim valor As Integer
        Dim totalItems As Integer = (final - inicial) / incremento

        If (LCase(sentido) = "crescente") Then
            valor = inicial
            While i <= totalItems
                lst_numeros_gerados.Items.Add(valor)
                valor = valor + incremento
                i = i + 1
            End While
        ElseIf (LCase(sentido) = "decrescente") Then
            valor = final
            While i <= totalItems
                lst_numeros_gerados.Items.Add(valor)
                valor = valor - incremento
                i = i + 1
            End While
        End If
    End Sub

    Private Sub btn_ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click

        lst_numeros_gerados.Items.Clear()

        If (rdb_for.Checked = True) Then
            iterateFor(CInt(txt_ni.Text), CInt(txt_nf.Text), CInt(txt_inc.Text), cmb_sentido.SelectedItem)
        ElseIf (rdb_while.Checked = True) Then
            iterateWhile(CInt(txt_ni.Text), CInt(txt_nf.Text), CInt(txt_inc.Text), cmb_sentido.SelectedItem)
        End If

    End Sub

    Private Sub frm_forwhile_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmb_sentido.Items.Add("crescente")
        cmb_sentido.Items.Add("decrescente")
    End Sub

End Class
