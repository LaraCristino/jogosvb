﻿Public Class frm_calculadora
    Private Function adicao(n1 As Integer, n2 As Integer)
        Return n1 + n2
    End Function
    Private Function subtracao(n1 As Integer, n2 As Integer)
        Return n1 - n2
    End Function
    Private Function multiplicacao(n1 As Integer, n2 As Integer)
        Return n1 * n2
    End Function
    Private Function divisao(n1 As Integer, n2 As Integer)
        Return n1 / n2
    End Function
    Private Sub btn_calcular_Click(sender As Object, e As EventArgs) Handles btn_calcular.Click
        Dim um As Integer = CInt(txt_um.Text)
        Dim dois As Integer = CInt(txt_dois.Text)

        If rdb_adicao.Checked = True Then
            lbl_resultado.Text = adicao(um, dois)
        ElseIf rdb_subtracao.Checked = True Then
            lbl_resultado.Text = subtracao(um, dois)
        ElseIf rdb_multiplicacao.Checked = True Then
            lbl_resultado.Text = multiplicacao(um, dois)
        ElseIf rdb_divisao.Checked = True Then
            lbl_resultado.Text = divisao(um, dois)
        End If

    End Sub



    Private Sub btn_limpa_Click(sender As Object, e As EventArgs) Handles btn_limpa.Click
        txt_um.Text = ""
        txt_dois.Text = ""
        lbl_resultado.Text = ""

    End Sub
End Class