﻿Public Class frm_paresimpares
    Dim num As Integer
    Dim resp As String
    Private Sub btn_mostrar_Click(sender As Object, e As EventArgs) Handles btn_mostrar.Click
        num = txt_num.Text
        If num Mod 2 = 0 Then
            resp = "Par"
            lbl_resp.Text = resp

        Else
            resp = "Impar"
            lbl_resp.Text = resp
        End If

    End Sub

    Private Sub btn_limpar_Click(sender As Object, e As EventArgs) Handles btn_limpar.Click
        txt_num.Clear()
        lbl_resp.Text = ""
        txt_num.Focus()

    End Sub
End Class